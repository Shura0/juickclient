var HelloCommandAssistant = function() {
}

var ConnectCommandAssistant = function() {
}

var AddCommandAssistant = function() {
}

var DelCommandAssistant = function() {
}

var StatusCommandAssistant = function() {
}

var version="0.22";
var RUN=0;
var sockets=new Array();
var FUTURE=0;
var SUBSCRIPTION=0;


CreateSocket = function (thread, future, subscription) {
  var o={
    socket:0,
    thread:thread,
    type:0,
    future:future,
    subscription:subscription
  };
  console.error("Creating new socket for "+thread);
  if(sockets[thread] && sockets[thread].socket)return;
  console.error("No socket for "+thread+" yet");
  //var ws=new exports.WebSocket('ws://api.juick.com:8080/replies/'+thread, 'sample');
  var ws=new exports.WebSocket('ws://ws.juick.com/'+thread, 'sample');
  o.type="reply";
  o.socket=ws;
  console.error("Socket created "+thread);
  (function(o){
  o.socket.onmessage = function(buf) {
    var f=o.subscription.get();
    f.result={reply:{reply:"ok",
                    type:o.type,
                    thread:o.thread,
                    message:buf}
              };
  };
  o.socket.onclose = function(buf)
  {
    console.error('socket closed');
    var f=o.subscription.get();
    f.result={reply:"socket closed"};
  }
  })(o);
  sockets[thread]=o;
  console.error("Socket created and marked "+thread);
};

HelloCommandAssistant.prototype.run = function(future) {
		//param=this.controller.args.name;
    future.result = {reply: "ok"};
};

StatusCommandAssistant.prototype.run = function(future) {
  var thread=this.controller.args.thread;
  if(sockets[thread] && sockets[thread].socket)
  {
    future.result={reply:sockets[thread].socket.readyState};
    return;
  }
  future.result={reply:-1};
};

AddCommandAssistant.prototype.run = function(future) {

  var thread=this.controller.args.thread;
  console.error("Trying to added thread "+thread);
  if(!(sockets[thread] && sockets[thread].socket))
  {
    console.error("It's new thread "+thread);
    if(FUTURE && SUBSCRIPTION)
    {
      console.error("Found connect! Attaching thread",thread);
      CreateSocket(thread, FUTURE, SUBSCRIPTION);
    }
    else
    {
      console.error("No any connects",thread);
    }
  }
  else
  {
    console.error("Socket already created",thread);
  }
  future.result={reply: {reply:"ok",
                          thread: thread}};
};

DelCommandAssistant.prototype.run = function(future) {
  var thread=this.controller.args.thread;
  if(!sockets[thread])
  {
    future.result={reply:{reply:"not found",
                          thread:thread}};
    return;
  }
  if(sockets[thread].socket && sockets[thread].socket.readyState == 1) //readyState is OPEN
  {
    sockets[thread].socket.close();
  }
    sockets[thread].socket=0;
    future.result={reply:{reply:"ok",
                          thread:thread}};
};

ConnectCommandAssistant.prototype.run = function(future, subscription) {
  console.error("Connect. future=",future, "subscription=",SUBSCRIPTION);
  for(var i in sockets)
  {
    CreateSocket(i,future,subscription);
  }
  FUTURE=future;
  SUBSCRIPTION=subscription;
  console.error("Connect. future=",future, "subscription=",SUBSCRIPTION);
  future.result = {reply:"ok"};
};

ConnectCommandAssistant.prototype.cancelSubscription = function() {
  console.error ("SUBSCRIPTION IS CANCELLED!!!");
  RUN=0;
  for(var i in sockets)
  {
    console.error ("unsubscribed from "+sockets[i].thread);
    sockets[i].socket.close();
    sockets[i]=0;
    delete sockets[i];
  }
};
