function MainAssistant(scene) {
  /* this is the creator function for your scene assistant object. It will be passed all the 
     additional parameters (after the scene name) that were passed to pushScene. The reference
     to the scene controller (this.controller) has not be established yet, so any initialization
     that needs the scene controller should be done in the setup function below. */
    if(scene)
      this.list=scene;
    else
      this.list="home";
     this.lastthread=0;
    Mojo.Log.info("p.list="+this.list);
}



MainAssistant.prototype.setup = function() {
  /* this function is for setup tasks that have to happen when the scene is first created */
    
  /* use Mojo.View.render to render view templates and add them to the scene, if needed */
  
  /* setup widgets here */
  this.messages="";
  this.controller.setupWidget("mainlistid",
        this.attributes = {
            itemTemplate: "list/listitem",
            listTemplate: "list/listcontainer",
            addItemLabel: "Ещё ...",
            hasNoWidgets: true,
            swipeToDelete: false,
            reorderable: false,
      fixedHeightItems:false,
      renderLimit: 2000,
            emptyTemplate:"list/emptylist",
            dividerTemplate:"none"
         },
         this.model = {
             listTitle: "Last posts",
       items: this.messages,
          }
    );
  this.controller.setupWidget(Mojo.Menu.appMenu,
    this.attributes = {
        omitDefaultItems: true
    },
    this.model = {
        visible: true,
        items: [ 
          { label: "Закладки", command: 'do-Bookmarks' },      
          { label: "Учетные данные", command: 'do-Prefs' },
          { label: "Настройки", command: 'do-Settings' },
          { label: "О программе", command: 'do-About' }
        ]
    }
  );
  this.mainmenu={
    visible: true,
    items: [ 
      { icon: 'compose', command: 'do-New'},
      { icon: '', label: "Home/Last", toggleCmd: 'do-Home',
    items: [
      //icons: suggestions for own blog: make-vip,remove-vip
      { icon: 'make-vip', label: "My", command: 'do-My'},
      { icon: 'home', label: "Home", command: 'do-Home'},
      { icon: 'last', label: "Last", command: 'do-Last'}
      ]
      },
      { icon: "refresh", command: "do-Refresh"},
    ]
  }
  var list=this.controller.get('mainlistid');
  if(this.list=="home")
  {
    this.mainmenu.items[1].toggleCmd='do-Home';
  }
  else
  {
    this.mainmenu.items[1].toggleCmd='do-New';
  }
  /* add event handlers to listen to events from widgets */
   
  this.controller.setupWidget("spinnerid",
        this.attributes = {
            spinnerSize: "small"
        },
        this.model = {
            spinning: true,
        }
    ); 
  this.listClickHandler_bind=this.listClickHandler.bindAsEventListener(this);
  this.listMoreClick_bind=this.listMoreClick.bind(this);
  this.controller.setupWidget(Mojo.Menu.commandMenu,undefined,this.mainmenu);
};

MainAssistant.prototype.activate = function(event) {
  /* put in event handlers here that should only be in effect when this scene is active. For
     example, key handlers that are observing the document */
  var list=this.controller.get('mainlistid');
  if(this.list=="home")
  {
    this.mainmenu.items[1].toggleCmd='do-Home';
  }
  else
  {
    this.mainmenu.items[1].toggleCmd='do-Last';
  }
  this.controller.modelChanged(this.mainmenu); 
  Mojo.Event.listen(this.controller.get('mainlistid'),
        Mojo.Event.listTap,
        this.listClickHandler_bind,true);
  Mojo.Event.listen(this.controller.get('mainlistid'),
    Mojo.Event.listAdd,
    this.listMoreClick_bind,true);
  if (this.list == "last")
    this.controller.get("headertext").update("Последние сообщения");
  if (!this.messages) {
    getData(this);
  }
};

MainAssistant.prototype.orientationChanged = function(orientation){
  Mojo.Log.info("Orientation Changed", orientation);
  this.controller.stageController.setWindowOrientation( orientation );
}
MainAssistant.prototype.deactivate = function(event) {
  /* remove any event handlers you added in activate and do any other cleanup that should happen before
     this scene is popped or another scene is pushed on top */
  Mojo.Event.stopListening(this.controller.get('mainlistid'),
        Mojo.Event.listTap,
        this.listClickHandler_bind);
  Mojo.Event.stopListening(this.controller.get('mainlistid'),
    Mojo.Event.listAdd,
    this.listMoreClick_bind,true);
    Mojo.Log.info("Main scene deactivated");

};

MainAssistant.prototype.cleanup = function(event) {
  /* this function should do any cleanup needed before the scene is destroyed as 
     a result of being popped off the scene stack */
};


MainAssistant.prototype.listClickHandler = function(event){
  
  var str=""+event.originalEvent.target;
  Mojo.Log.info("tap on "+ str);
  if(str.match(/^http[s]?:/))
  {
    Mojo.Log.info("returning");
    return;
  }
  else if(str.match(/HTMLImageElement/))
  {
    var src=event.originalEvent.target.src;
    if(src.match(/^file:/))return; // local file, impossible to show.
    if(isTouchpad())
    {
      src=src.replace("/photos-1024/","/p/");
      src=src.replace("/photos-512/","/p/");
    }
    else
    {
      src=src.replace("/ps/","/photos-1024/");
    }
    Mojo.Log.info("image is "+ src);
    this.controller.serviceRequest("palm://com.palm.applicationManager", {
    method: "launch",
    parameters: {
        id: 'com.palm.app.browser',
        params: {
            "target": src
        }
      }});   
    Mojo.Log.info("returning");
    return;
  }
  else if(str.match(/^file:/)) //our internal links
  {
    var index=str.indexOf("#username:");
    if (index > 0) {
      var name = str.substr(index + 11); //11- length of #username:@
      Mojo.Log.info("username:", name);
      return;
    }
    index=str.indexOf("#thread:");
    if(index>0){
      var threada=str.substr(index+11); //11 - length of #thread:%23
      Mojo.Log.info("thread #"+threada);
      Mojo.Controller.stageController.pushScene("Post",
                        {thread:threada,
                        username: "",
                        userid:0});
      return;
    }
  }
  Mojo.Controller.stageController.pushScene({name:"Post"},
                        {thread:event.item.mid,
                        username: event.item.user.uname,
                        userid:event.item.user.uid});

}

MainAssistant.prototype.listMoreClick = function(event){
  this.more=1;
  com = new Object();
  com.command = "do-Refresh";
  com.type = "mojo-command";
  com.sender=this; 
  StageAssistant.prototype.handleCommand(com);
}
