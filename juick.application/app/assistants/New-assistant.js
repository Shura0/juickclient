function NewAssistant(p) {
  /* this is the creator function for your scene assistant object. It will be passed all the
     additional parameters (after the scene name) that were passed to pushScene. The reference
     to the scene controller (this.controller) has not be established yet, so any initialization
     that needs the scene controller should be done in the setup function below. */
     if(p)
     {
       this.username=p.username;
       this.thread=p.thread;
       this.replyid=p.replyid;
       Mojo.Log.info("thread="+this.thread,"username="+this.username,"replyid="+this.replyid);
     }
}

NewAssistant.prototype.setup = function() {
  /* this function is for setup tasks that have to happen when the scene is first created */

  /* use Mojo.View.render to render view templates and add them to the scene, if needed */

  /* setup widgets here */

  /* add event handlers to listen to events from widgets */
  this.placechanged=0;
  this.places=new Array();
  this.placesModel={
        value: 0,
        disabled: false,
        choices:this.places
      }
  this.controller.setupWidget("newtext",
          this.attributes = {
              multiline: true,
              autoResizeMax: true,
              enterSubmits: false,
              focus: true,
              changeOnKeyPress: false,
          },
          this.model = {
              value: "",
              disabled: false
     });
   this.controller.setupWidget("spinnerid",
        this.attributes = {
            spinnerSize: "small"
        },
        this.model = {
            spinning: false,
        }
    );

        this.controller.setupWidget("placesList",
        this.attributes = {
          multiline:true
        },
        this.model=this.placesModel
    );

    if(Settings.use_gps && this.thread==0)
    {
      this.controller.get("placesList").style.display="block";
      this.controller.get("placeText").style.display="block";
    }
    else
    {
      this.controller.get("placesList").style.display="none";
      this.controller.get("placeText").style.display="none";
    }
    this.commandMenuModel= newscene.commandMenuModel;
    this.commandMenuModel.visible=true;
  this.controller.setupWidget(Mojo.Menu.commandMenu,
    undefined,
    this.commandMenuModel);
  if(isTouchpad())
  {
    this.controller.get("back").style.display="block";
    this.controller.setupWidget("back",
    this.attributes = {
    },
    this.model = {
        label : "Назад",
        disabled: false
        }
      );
    this.backClick_bind=this.backClick.bindAsEventListener(this);
  }
  this.placeChange_bind=this.placeChange.bindAsEventListener(this);

};

NewAssistant.prototype.activate = function(event) {
  /* put in event handlers here that should only be in effect when this scene is active. For
     example, key handlers that are observing the document */

  if(isTouchpad())
    Mojo.Event.listen(this.controller.get("back"),Mojo.Event.tap, this.backClick_bind,true);
  if(Settings.use_gps && this.thread==0)
  {
    this.places=[];
    var obj=new Object();
    obj.label="[НЕТ]";
    obj.value=0;
    this.places.push(obj);
    Mojo.Log.info(JSON.stringify(obj));
    for (var i=0;i<Settings.places.length;i++)
    {
      obj=new Object();
      obj.label=Settings.places[i].name;
      obj.value=Settings.places[i].pid;
      this.places.push(obj);
      Mojo.Log.info(JSON.stringify(obj));
    }
    this.places.push({label:"[Редактировать]",value:-1});
    this.placesModel.choices=this.places;
    this.placesModel.value=0;
    Mojo.Log.info(JSON.stringify(this.places));
    this.controller.modelChanged(this.placesModel);
    self=this;
    this.controller.serviceRequest('palm://com.palm.location', {
      method: 'getCurrentPosition',
      parameters: {},
      onSuccess : function (e){ Mojo.Log.info("getCurrentPosition success, results="+JSON.stringify(e));
        var lat=e.latitude;
        var lon=e.longitude;
        var minlen=100;
        var curposition=0;
        for (var i=0;i<Settings.places.length;i++)
        {
          var lat1=Settings.places[i].lat;
          var lon1=Settings.places[i].lon;
          len=Math.sqrt((lat-lat1)*(lat-lat1)+(lon-lon1)*(lon-lon1));
          Mojo.Log.info("Place="+Settings.places[i].name+" Length="+len);
          if(len<0.01 && len<minlen)
          {
            minlen=len;
            curposition=Settings.places[i].pid;
            Mojo.Log.info("Found place!!! Place="+Settings.places[i].name+" Length="+len);
          }
        }
        if(minlen<100 && self.placechanged==0)
        {
          self.placesModel.choices=self.places;
          self.placesModel.value=curposition;
          self.controller.modelChanged(self.placesModel);
          var list=self.controller.get("placesList");
          list.style.color="green";
          Location_id=curposition;
        }
      },
      onFailure : function (e){
        Mojo.Log.info("getCurrentPosition failure, results="+JSON.stringify(e));
        //Mojo.Controller.errorDialog("Невозможно определить текущее местоположение");
        //this.widget.mojo.close();
      }
    });
  }

  if(!checkAuth())
  {
    getAuth();
  }

  Mojo.Event.listen(this.controller.get("placesList"),Mojo.Event.propertyChange, this.placeChange_bind,true);
  if(this.thread==0)return;
    var text="#"+this.thread;
    if(this.replyid!="0")
    {
      text+="/"
      text+=this.replyid;
    }
    var tt=this.controller.get("newtext");
    var hh=this.controller.get("headertext");
    hh.update("To "+newscene.user);
    if(tt.mojo.getValue()=="")
      tt.mojo.setValue(text+" ");
    //~ Mojo.Event.listen(this.controller.get('headerid'),
        //~ Mojo.Event.tap,
        //~ this.headerClickHandler_bind,true);




    Mojo.Log.info("end of new scene activating");

};

NewAssistant.prototype.deactivate = function(event) {

  if(isTouchpad())
  Mojo.Event.stopListening(this.controller.get('back'),
        Mojo.Event.tap,
        this.backClick_bind);
        this.attach="";
        this.thread=0;
  Mojo.Event.stopListening(this.controller.get("placesList"),Mojo.Event.propertyChange, this.placeChange_bind,true);
};

NewAssistant.prototype.cleanup = function(event) {
  /* this function should do any cleanup needed before the scene is destroyed as
     a result of being popped off the scene stack */
};

NewAssistant.prototype.headerClickHandler = function(event){
  //Mojo.Controller.stageController.pushScene("User", this);
}
NewAssistant.prototype.backClick = function(event) {
  Mojo.Controller.stageController.popScene();
  //closeScene();
};

NewAssistant.prototype.placeChange = function (event) {
  if (event.value==-1)
  {
    Mojo.Controller.stageController.pushScene("Places");
  }
  else
  {
    Location_id=event.value;
  }
  this.placechanged=1;
  var list=this.controller.get("placesList");
  list.style.color="black";
}

