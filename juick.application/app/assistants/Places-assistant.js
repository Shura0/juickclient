
function PlacesAssistant() {
  /* this is the creator function for your scene assistant object. It will be passed all the 
     additional parameters (after the scene name) that were passed to pushScene. The reference
     to the scene controller (this.controller) has not be established yet, so any initialization
     that needs the scene controller should be done in the setup function below. */
}

PlacesAssistant.prototype.setup = function() {
  /* this function is for setup tasks that have to happen when the scene is first created */
    
  /* use Mojo.View.render to render view templates and add them to the scene, if needed */
  
  /* setup widgets here */
  
  this.controller.setupWidget("mainlistid",
        this.attributes = {
            itemTemplate: "list/placeslistitem",
            listTemplate: "list/listcontainer",
            swipeToDelete: true,
						hasNoWidgets: true,
            reorderable: true,
      fixedHeightItems:false,
      renderLimit: 256,
            emptyTemplate:"list/emptylist",
         },
         this.model = {
             listTitle: "Места",
              items: Settings.places,
          }
    );
  this.controller.setupWidget(Mojo.Menu.appMenu,
    this.attributes = {
        omitDefaultItems: true
    },
    this.model = {
        visible: true,
        items: [ 
            { label: "Удалить все", command: 'do-ClearPlaces' },
        ]
    }
    );
  /* add event handlers to listen to events from widgets */
  if(isTouchpad())
  {
    this.controller.get("back").style.display="block";
    this.controller.setupWidget("back",
      this.attributes = {
      },
      this.model = {
        label : "Назад",
        disabled: false
      }
    );
    this.controller.get("back").style.display="block";
    this.backClick_bind=this.backClick.bindAsEventListener(this);
  }
  this.controller.setupWidget(Mojo.Menu.appMenu,
    this.attributes = {
        omitDefaultItems: true
    },
    this.model = {
        visible: true,
    }
    );
		this.mainmenu={
      visible: true,
      items: [ 
        { icon: "new", command: "do-AddPlace"}
      ]
    }
  this.controller.setupWidget(Mojo.Menu.commandMenu,undefined,this.mainmenu);
   
  this.listClickHandler_bind=this.listClickHandler.bindAsEventListener(this);
  this.listDeleteHandler_bind=this.listDeleteHandler.bindAsEventListener(this);
  this.listReorderHandler_bind=this.listReorderHandler.bindAsEventListener(this);
};

PlacesAssistant.prototype.activate = function(event) {
  /* put in event handlers here that should only be in effect when this scene is active. For
     example, key handlers that are observing the document */
  
  if(Settings.places.length>0)
  {
    this.controller.get('mainlistid').mojo.noticeUpdatedItems(0,Settings.places);
    Mojo.Log.info(JSON.stringify(Settings.places));
  }
  Mojo.Event.listen(this.controller.get('mainlistid'),
        Mojo.Event.listTap,
        this.listClickHandler_bind,true);
    Mojo.Event.listen(this.controller.get('mainlistid'),
        Mojo.Event.listDelete,
        this.listDeleteHandler_bind,true);
    //Mojo.Event.listen(this.controller.get('mainlistid'),
    //Mojo.Event.listReorder,
        //this.listReorderHandler_bind,true);
  if(isTouchpad())
    Mojo.Event.listen(this.controller.get("back"),Mojo.Event.tap, this.backClick_bind,true);   
  
};

PlacesAssistant.prototype.orientationChanged = function(orientation){
  Mojo.Log.info("Orientation Changed", orientation);
  this.controller.stageController.setWindowOrientation( orientation );
}
PlacesAssistant.prototype.deactivate = function(event) {
  /* remove any event handlers you added in activate and do any other cleanup that should happen before
     this scene is popped or another scene is pushed on top */
  Mojo.Event.stopListening(this.controller.get('mainlistid'),
        Mojo.Event.listTap,
        this.listClickHandler_bind);
    Mojo.Event.stopListening(this.controller.get('mainlistid'),
        Mojo.Event.listDelete,
        this.listDeleteHandler_bind);
    //Mojo.Event.stopListening(this.controller.get('mainlistid'),
        //Mojo.Event.listReorder,
        //this.listReorderHandler_bind);
  if(isTouchpad())
    Mojo.Event.stopListening(this.controller.get('back'),
        Mojo.Event.tap,
        this.backClick_bind);
};

PlacesAssistant.prototype.cleanup = function(event) {
  /* this function should do any cleanup needed before the scene is destroyed as 
     a result of being popped off the scene stack */
};

PlacesAssistant.prototype.listClickHandler = function(event){
  /*var str=""+event.originalEvent.target;
  Mojo.Controller.stageController.pushScene("Post", { thread:event.item.thread,
                            username:"",
                            userid:0}
                            );
  */
  var lat=Settings.places[event.index].lat;
  var lon=Settings.places[event.index].lon;
  var name=Settings.places[event.index].name;
  var val='{lat:"'+lat+'",lon:"'+lon+'"}';
  var params = [{
		// convert the coordinates (from German into international format)
		lat: lat,
		lon: lon,
		// give the spot a name and optional specify an url and icon
		name: name
	}];
  var self=this;
  this.controller.serviceRequest('palm://com.palm.applicationManager', {
    method: 'launch',
    parameters: {
      id: 'de.metaviewsoft.maptoolpro',
      params: params
    },
    onFailure: function() {
			self.controller.serviceRequest('palm://com.palm.applicationManager', {
				method: "open",
				parameters: {
					id: 'de.metaviewsoft.maptool',
					params: params
				}
      });
    }
  });
}

PlacesAssistant.prototype.listDeleteHandler = function(event){
  //WebSocketTest(event.item.thread,"disconnect");
  Settings.places.splice(event.index,1);
  saveSettings();
}

PlacesAssistant.prototype.listReorderHandler = function(event){
  var from=event.fromIndex;
  var to=event.toIndex;
  var temp=Settings.places[from];
  Settings.places.splice(from,1);
  Settings.places.splice(to,0,temp);
  saveSettings();
}
PlacesAssistant.prototype.backClick = function(event) {
  Mojo.Controller.stageController.popScene();
  //closeScene();
};
