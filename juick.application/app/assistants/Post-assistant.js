function PostAssistant(p) {
  /* this is the creator function for your scene assistant object. It will be passed all the 
     additional parameters (after the scene name) that were passed to pushScene. The reference
     to the scene controller (this.controller) has not be established yet, so any initialization
     that needs the scene controller should be done in the setup function below. */
     this.thread=0;
     this.username="";
     this.userid=0;
    this.reply=0;
     if(p)
     {
      this.thread=p.thread;
      this.username=p.username;
      this.userid=p.userid;
      this.reply=p.reply;
      Mojo.Log.info("thread="+this.thread,"username="+this.username,"userid="+this.userid, "reply="+this.reply);
     }
     
};

var stopevent=0;

PostAssistant.prototype.setup = function() {
  /* this function is for setup tasks that have to happen when the scene is first created */
    
  /* use Mojo.View.render to render view templates and add them to the scene, if needed */
  
  /* setup widgets here */
  
  /* add event handlers to listen to events from widgets */
  this.controller.setupWidget("mainlistid",
        this.attributes = {
            itemTemplate: "list/threadlistitem",
            listTemplate: "list/listcontainer",
            //addItemLabel: "Add ...",
            swipeToDelete: false,
            reorderable: false,
      fixedHeightItems:false,
      renderLimit: 1024,
            emptyTemplate:"list/emptylist",
      },
         this.model = {
             listTitle: "Last posts",
             items : [
                 {data: "Loading..."},
             ]
          }
    );
  
  this.mainmenu={
            visible: true,
        items: [ 
            { icon: "priority", command: "do-Send"},
            { icon: "refresh", command: 'do-Refresh'}
        ]
    }
  this.controller.setupWidget("spinnerid",
        this.attributes = {
            spinnerSize: "small"
        },
        this.model = {
            spinning: true,
        }
    );
    
    this.controller.setupWidget(Mojo.Menu.appMenu,
    this.attributes = {
        omitDefaultItems: true
    },
    this.model = {
        visible: true,
        items: [ 
            { label: "Добавить в закладки", command: 'do-AddBookmark' },
        ]
    }
);
    if(isTouchpad())
    {
      this.controller.get("back").style.display="block";
      this.controller.setupWidget("back",
        this.attributes = {
        },
        this.model = {
          label : "Назад",
          disabled: false
        }
      );
      this.backClick_bind=this.backClick.bindAsEventListener(this);
    }
  
  /* add event handlers to listen to events from widgets */
  this.listClickHandler_bind=this.listClickHandler.bind(this);
  this.headerClickHandler_bind=this.headerClickHandler.bind(this);
  this.controller.setupWidget(Mojo.Menu.commandMenu,undefined,this.mainmenu);
};

PostAssistant.prototype.activate = function(event) {
  
  var spin=this.controller.get('spinnerid');
  spin.mojo.start();
  spin.style.visible="block";
  getData(this);
  Mojo.Event.listen(this.controller.get('mainlistid'),
        Mojo.Event.listTap,
        this.listClickHandler_bind,true);
  if(isTouchpad())
    Mojo.Event.listen(this.controller.get("back"),Mojo.Event.tap, this.backClick_bind,true); 
  Mojo.Event.listen(this.controller.get('headerid'),
        Mojo.Event.tap,
        this.headerClickHandler_bind,true);
  
  //checking for websocket status
  var status=this.controller.get('ws_status');
  status.style.display="none";
  Mojo.Log.info("getting post websocket status");
  if ("WebSocket" in window)
  {
    if(wsthread[this.thread])
    {
      switch (wsthread[this.thread].socket.readyState)
      {
        case 1: //open
          status.style.backgroundImage="url('images/green_point.png')";
        break;
        case 2: //closing
        case 0: //connecting
          status.style.backgroundImage="url('images/yellow_point.png')";
        break;
        case 3: //closed
          status.style.backgroundImage="url('images/red_point.png')";
        break;
      }
      status.style.display="inline";
    }
  }
  else
  {
    Mojo.Log.info("websocket service");
    this.controller.serviceRequest("palm://ru.ya.shura0.juickclient.service", {
        method: "status",
        parameters: {thread:this.thread },
        onSuccess: function(data){
          Mojo.Log.info("got status:",data.reply);
          Mojo.Log.info("thread=",this.thread,"wsthread=",wsthread[this.thread]);
          status.style.display="inline";
          switch (data.reply)
          {
            case -1:
              status.style.display="none";
            break;
            case 1: //open
              status.style.backgroundImage="url('images/green_point.png')";
            break;
            case 2: //closing
            case 0: //connecting
              status.style.backgroundImage="url('images/yellow_point.png')";
            break;
            case 3: //closed
              status.style.backgroundImage="url('images/red_point.png')";
            break;
          }
        },
    });
  }
  
  /* put in event handlers here that should only be in effect when this scene is active. For
     example, key handlers that are observing the document */
};

PostAssistant.prototype.deactivate = function(event) {
  /* remove any event handlers you added in activate and do any other cleanup that should happen before
     this scene is popped or another scene is pushed on top */
    Mojo.Event.stopListening(this.controller.get('mainlistid'),
        Mojo.Event.listTap,
        this.listClickHandler_bind);
    Mojo.Event.stopListening(this.controller.get('headerid'),
        Mojo.Event.tap,
        this.headerClickHandler_bind,true);
  if(isTouchpad())
    Mojo.Event.stopListening(this.controller.get('back'),
        Mojo.Event.tap,
        this.backClick_bind);
};

PostAssistant.prototype.cleanup = function(event) {
  /* this function should do any cleanup needed before the scene is destroyed as 
     a result of being popped off the scene stack */
};


PostAssistant.prototype.listClickHandler = function(event){
  var str=""+event.originalEvent.target;
  Mojo.Log.info("tap on "+ str);
  if(str.match(/^http/))
  {
    return;
  }
  else if(str.match(/^file:/)) //our internal links
  {
    var index=str.indexOf("#username:");
    if (index > 0) {
      var name = str.substr(index + 10); //10- length of #username:
      Mojo.Log.info("username:", name);
      return;
    }
    index=str.indexOf("#thread:");
    if(index>0){
      var threada=str.substr(index+11); //11 - length of #thread:%23
      Mojo.Log.info("thread #"+threada);
      Mojo.Controller.stageController.pushScene("Post", {thread:threada,
                                username:"",
                                userid:0});
      return;
    }
  }
  else if(str.match(/HTMLImageElement/))
  {
    var src=event.originalEvent.target.src;
    if(src.match(/^file:/))return; // local file, impossible to show.
    if(isTouchpad())
    {
      src=src.replace("/photos-1024/","/p/");
      src=src.replace("/photos-512/","/p/");
    }
    else
    {
      src=src.replace("/ps/","/photos-1024/");
    }
    Mojo.Log.info("image is "+ src);
    this.controller.serviceRequest("palm://com.palm.applicationManager", {
    method: "launch",
    parameters: {
        id: 'com.palm.app.browser',
        params: {
            "target": src
        }
      }});   
    Mojo.Log.info("returning");
    return;
  }
  newscene.user=event.item.user.uname;
  newscene.thread=event.item.mid;
  Mojo.Log.info("event.item.user.uname=",event.item.user.uname)
  var rid=0;
  if (event.item.rid) {
    rid=event.item.rid;
  }
  else{
    newscene.replyid=0;
  }
  Mojo.Controller.stageController.pushScene("New", {
                          thread:event.item.mid,
                          replyid:rid,
                          username:this.username});
}

PostAssistant.prototype.headerClickHandler = function(event){ //going to userscene
  Mojo.Log.info("Header clicked!");
  Mojo.Controller.stageController.pushScene("User", {username:this.username,
                            userid:this.userid});
}
PostAssistant.prototype.backClick = function(event) {
  //closeScene();
  Mojo.Log.info("back clicked!");
  Mojo.Controller.stageController.popScene(this.controller,this);
  Mojo.Controller.stageController.popScene(this.controller,this);
};

