
var STATE;

function BlacklistAssistant(scene) {
  /* this is the creator function for your scene assistant object. It will be passed all the 
     additional parameters (after the scene name) that were passed to pushScene. The reference
     to the scene controller (this.controller) has not be established yet, so any initialization
     that needs the scene controller should be done in the setup function below. */
}



BlacklistAssistant.prototype.setup = function() {
  /* this function is for setup tasks that have to happen when the scene is first created */
    
  /* use Mojo.View.render to render view templates and add them to the scene, if needed */
  
  /* setup widgets here */
  this.activestate='do-tags';
  STATE='do-tags';
  this.controller.setupWidget("mainlistid",
        this.attributes = {
            itemTemplate: "list/blacklistitem",
            listTemplate: "list/listcontainer",
            swipeToDelete: true,
						hasNoWidgets: true,
            reorderable: true,
      fixedHeightItems:true,
      renderLimit: 256,
            emptyTemplate:"list/emptylist",
         },
         this.model = {
             listTitle: "Чёрный список",
             // items: Settings.blacklist_tag,
          }
    );
  if(isTouchpad())
  {
    this.controller.setupWidget(Mojo.Menu.viewMenu,
      this.attributes = {
        spacerHeight: 0,
        menuClass: 'no-fade'
      },
      this.model = {
        visible: true,
        items: [
          {label:"<-Back",command: 'do-back'},
          {label:"",toggleCmd:'do-tags',
            items:[
              { label: 'теги', command: 'do-tags', width:160},
              { label: 'авторы',command: 'do-authors', width:160}
            ]
          }
        ]
      }
    ); 
  }
  else{
    this.controller.setupWidget(Mojo.Menu.viewMenu,
      this.attributes = {
        spacerHeight: 0,
        menuClass: 'no-fade'
      },
      this.model = {
        visible: true,
        items: [
          {label:"hren",toggleCmd:'do-tags',
            items:[
              {label: 'теги', command: 'do-tags', width:160},
              {label: 'авторы',command: 'do-authors', width:160}
            ]
          }
        ]
      }
    ); 
  }
  this.controller.setupWidget(Mojo.Menu.appMenu,
    this.attributes = {
        omitDefaultItems: true
    },
    this.model = {
        visible: true,
    }
    );
		this.mainmenu={
            visible: true,
        items: [ 
            { icon: "new", command: "do-Addblack"}
        ]
    }
  /* add event handlers to listen to events from widgets */
  this.listDeleteHandler_bind=this.listDeleteHandler.bindAsEventListener(this);
  this.listReorderHandler_bind=this.listReorderHandler.bindAsEventListener(this);
	this.controller.setupWidget(Mojo.Menu.commandMenu,undefined,this.mainmenu);
};

BlacklistAssistant.prototype.activate = function(event) {
  /* put in event handlers here that should only be in effect when this scene is active. For
     example, key handlers that are observing the document */
  Mojo.Log.info(this.activestate);
  switch (STATE)
  {
    case 'do-tags':
      //if(Settings.blacklist_tag && Settings.blacklist_tag.length>0)
      {
      this.controller.get('mainlistid').mojo.setLength(0);
      this.controller.get('mainlistid').mojo.noticeUpdatedItems(0,Settings.blacklist_tag);
      Mojo.Log.info(JSON.stringify(Settings.blacklist_tag));
      }
    break;
    case 'do-authors':
      //if(Settings.blacklist_author && Settings.blacklist_author.length>0)
      {
        this.controller.get('mainlistid').mojo.setLength(0);
        this.controller.get('mainlistid').mojo.noticeUpdatedItems(0,Settings.blacklist_author);
        Mojo.Log.info(JSON.stringify(Settings.blacklist_author));
      }
    break;
  }
    Mojo.Event.listen(this.controller.get('mainlistid'),
        Mojo.Event.listDelete,
        this.listDeleteHandler_bind,true);
    Mojo.Event.listen(this.controller.get('mainlistid'),
    Mojo.Event.listReorder,
        this.listReorderHandler_bind,true);
  if(isTouchpad())
    Mojo.Event.listen(this.controller.get("back"),Mojo.Event.tap, this.backClick_bind,true);
	
};

BlacklistAssistant.prototype.orientationChanged = function(orientation){
  Mojo.Log.info("Orientation Changed", orientation);
  this.controller.stageController.setWindowOrientation( orientation );
}
BlacklistAssistant.prototype.deactivate = function(event) {
  /* remove any event handlers you added in activate and do any other cleanup that should happen before
     this scene is popped or another scene is pushed on top */
    Mojo.Event.stopListening(this.controller.get('mainlistid'),
        Mojo.Event.listDelete,
        this.listDeleteHandler_bind);
    Mojo.Event.stopListening(this.controller.get('mainlistid'),
        Mojo.Event.listReorder,
        this.listReorderHandler_bind);
  if(isTouchpad())
    Mojo.Event.stopListening(this.controller.get('back'),
        Mojo.Event.tap,
        this.backClick_bind);
};

BlacklistAssistant.prototype.cleanup = function(event) {
  /* this function should do any cleanup needed before the scene is destroyed as 
     a result of being popped off the scene stack */
};

BlacklistAssistant.prototype.listDeleteHandler = function(event){
  //WebSocketTest(event.item.thread,"disconnect");
  if(STATE == 'do-tags')
    Settings.blacklist_tag.splice(event.index,1);
  else
    Settings.blacklist_author.splice(event.index,1);
  saveSettings();
}

BlacklistAssistant.prototype.listReorderHandler = function(event){
  var from=event.fromIndex;
  var to=event.toIndex;
  if(STATE == 'do-tags')
  {
    var temp=Settings.blacklist_tag[from];
    Settings.blacklist_tag.splice(from,1);
    Settings.blacklist_tag.splice(to,0,temp);
  }
  else
  {
    var temp=Settings.blacklist_author[from];
    Settings.blacklist_author.splice(from,1);
    Settings.blacklist_author.splice(to,0,temp);
  }
  saveSettings();
}
BlacklistAssistant.prototype.backClick = function(event) {
  Mojo.Controller.stageController.popScene();
  //closeScene();
}

BlacklistAssistant.addWord=function()
{
  Mojo.Log.info("STATE="+STATE);
  Mojo.Controller.stageController.pushScene("Addblack",{state:STATE});
}

BlacklistAssistant.switchList=function(state) //called from stageAssistant (menu handler)
{
  Mojo.Log.info("swithList",state);
  this.controller=Mojo.Controller.stageController.activeScene();
  switch (state)
  {
    case 'do-tags':
      //if(Settings.blacklist_tag && Settings.blacklist_tag.length>0)
      {
        this.controller.get('mainlistid').mojo.setLength(0);
        this.controller.get('mainlistid').mojo.noticeUpdatedItems(0,Settings.blacklist_tag);
        Mojo.Log.info(JSON.stringify(Settings.blacklist_tag));
      }
    break;
    case 'do-authors':
      //if(Settings.blacklist_author && Settings.blacklist_author.length>0)
      {
        this.controller.get('mainlistid').mojo.setLength(0);
        this.controller.get('mainlistid').mojo.noticeUpdatedItems(0,Settings.blacklist_author);
        Mojo.Log.info(JSON.stringify(Settings.blacklist_author));
      }
    break;
  }
  this.activestate=state;
  STATE=state;
};


