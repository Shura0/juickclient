
function DashboardAssistant(m, count) {
  /* this is the creator function for your scene assistant object. It will be passed all the 
     additional parameters (after the scene name) that were passed to pushScene. The reference
     to the scene controller (this.controller) has not be established yet, so any initialization
     that needs the scene controller should be done in the setup function below. */
  Mojo.Log.info("Dashboard: Message:"+m.message.body);
  this.message=m;
  this.messages=new Array();
  this.messages.push(m);
  
}

DashboardAssistant.prototype.setup = function() {
  var d=this.controller.get("dashboardinfo");
  d.update("@"+this.message.message.user.uname+": "+this.message.message.body);
  if(this.message.type =="all")
  {
    var n=this.controller.get("allimage");
    n.style.display="inline";
  }
  else
  {
    var n=this.controller.get("rimage");
    n.style.display="inline";
  }
  Mojo.Controller.getAppController().playSoundNotification("vibrate","");
  this.tapHandler_bind=this.tapHandler.bindAsEventListener(this);
  Mojo.Event.listen(this.controller.get('dashboardinfo'),
        Mojo.Event.tap,
        this.tapHandler_bind,true);
}
  
DashboardAssistant.prototype.updateDashboard = function(m, count) {
  var info = {message: m.message.body, count: count};
  // Use render to convert the object and its properties along with a view file
  // into a string containing HTML
  //var renderedInfo = Mojo.View.render({object: info, template: 'Dashboard/item-info'});
  //var infoElement = this.controller.get('allcounter');
  //infoElement.update(renderedInfo);
  //infoElement.update("10");
  infoElement = this.controller.get('rcounter');
  if(count>0)
    infoElement.style.display="inline";
  infoElement.update(count);
  Mojo.Controller.getAppController().playSoundNotification("vibrate","");
  this.messages.push(m);
  var ali,ri;
  ali=this.controller.get("allimage");
  ri=this.controller.get("rimage");
  var d=this.controller.get("dashboardinfo");
  //d.update(renderedInfo);
  d.update("@"+m.message.user.uname+": "+m.message.body);
  if(m.type=="all")
  {
    ali.style.display="inline";
  }
  else
  {
    ri.style.display="inline";
  }
  if(ali.style.display=="inline" && ri.style.display=="inline")
  {
    var n=this.controller.get("dashboardinfo");
    n.style.width="200px";
  }
  var acounter=this.controller.get("allcounter");
  acounter.update(count);
  acounter.style.display="block";
  Mojo.Log.info("messages :"+this.messages.length);
};

DashboardAssistant.prototype.launchMain = function() {
  var appController = Mojo.Controller.getAppController();
  appController.assistant.handleLaunch({source:"notification"});
  this.controller.window.close();
};

DashboardAssistant.prototype.tapHandler = function(event)
{
  var m=this.messages.pop();
  if(!m) return;
  Mojo.Log.info("first message is: "+m.message.body);
  Mojo.Log.info("messages left:"+this.messages.length);
  if(m.type=="all")
  {
    var a=new Array();
    //delete all "all" messages
    for(i=0;i<this.messages.length;i++)
    {
      if(this.messages[i].type !="all")
        a.push(this.messages[i]);
    }
    var n=this.controller.get("allimage");
    n.style.display="none";
    n=this.controller.get("dashboardinfo");
    n.style.width="252px";
    this.messages=a;
    var name=Mojo.Controller.stageController.activeScene().sceneName;
    if(name !="New") //we don't loose unfinished new message
    {
      Mojo.Controller.stageController.popScene();
      Mojo.Controller.stageController.swapScene("Main","last");
    }else{
        Mojo.Controller.stageController.pushScene("Main","last");
    }
  }
  else //reply message
  {
    var a=new Array();
    //delete all reply messages from this thread
    var flag=0;
    for(i=0;i<this.messages.length;i++)
    {
      if(this.messages[i].type !="all")
      {
        if(this.messages[i].message.thread == m.message.thread)
        {
          continue;
        }
        flag=1;
      }
      a.push(this.messages[i]);
    }
    this.messages=a;
    if(!flag) //hide image with replies
    {
      var n=this.controller.get("rimage");
      n.style.display="none";
      n=this.controller.get("dashboardinfo");
      n.style.width="252px";
    }
    var name=Mojo.Controller.stageController.activeScene().sceneName;
    Mojo.Controller.stageController.pushScene("Post",{thread:m.message.mid,username:"",userid:0,reply:m.message.rid});
  }
  //update dashboard
  if(this.messages.length>0)
  {
    m=this.messages[this.messages.length-1]
    var d=this.controller.get("dashboardinfo");
    d.update("@"+m.message.user.uname+": "+m.message.body);
  }
  else
    this.controller.window.close();
}