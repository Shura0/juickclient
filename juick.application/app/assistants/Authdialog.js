var CustomDialogAssistant = Class.create({
  initialize: function(sceneAssistant) {
    this.sceneAssistant = sceneAssistant;
    this.controller = sceneAssistant.controller;
  },
  setup : function(widget) {
    this.widget = widget;
    var lt=Settings.login;
    var pt=Settings.password;
    this.controller.setupWidget("loginid",
      this.attributes = {
        hintText: "Login",
        multiline: false,
        enterSubmits: false,
        autoReplace: false,
        textCase: Mojo.Widget.steModeLowerCase,
        focus: true
      },
      this.model = {
        value: lt,
        disabled: false
      }
    );
    this.controller.setupWidget("passid",
      this.attributes = {
        hintText: "password",
        enterSubmits: false,
        focus: false,
      },
      this.model = {
        value: pt,
        disabled: false
      }
    );
    this.controller.setupWidget("ok",
      this.attributes = {
      },
      this.model = {
        label : "Ok",
        disabled: false
      }
    );
    this.controller.setupWidget("cancel",
      this.attributes = {
      },
      this.model = {
        label : "Cancel",
        disabled: false,
        buttonClass:"negative",
      }
    );
    this.controller.get('ok').addEventListener(Mojo.Event.tap,
      this.handleButtonok.bind(this));
    this.controller.get('cancel').addEventListener(Mojo.Event.tap,
      this.handleButton.bind(this));
  },
  handleButton: function() {
    this.widget.mojo.close();
  },
  handleButtonok: function() {
    Settings.login=this.controller.get('loginid').mojo.getValue();
    Settings.password=this.controller.get('passid').mojo.getValue();
    getUserId();
    saveSettings();
    this.widget.mojo.close();
  }
});
