function AboutAssistant() {
  /* this is the creator function for your scene assistant object. It will be passed all the 
     additional parameters (after the scene name) that were passed to pushScene. The reference
     to the scene controller (this.controller) has not be established yet, so any initialization
     that needs the scene controller should be done in the setup function below. */
}



AboutAssistant.prototype.setup = function() {
  /* this function is for setup tasks that have to happen when the scene is first created */
    
  /* use Mojo.View.render to render view templates and add them to the scene, if needed */
  
  /* setup widgets here */
  if(isTouchpad())
  {
    this.controller.get("back").style.display="block";
    this.controller.setupWidget("back",
    this.attributes = {
    },
    this.model = {
        label : "Назад",
        disabled: false
    }
  );
    this.backClick_bind=this.backClick.bindAsEventListener(this);
  }
  
};

AboutAssistant.prototype.activate = function(event) {
  this.controller.get("ver").update("v."+Mojo.appInfo.version);
  if(isTouchpad())
    Mojo.Event.listen(this.controller.get("back"),Mojo.Event.tap, this.backClick_bind,true); 
};

AboutAssistant.prototype.orientationChanged = function(orientation){
  Mojo.Log.info("Orientation Changed", orientation);
  this.controller.stageController.setWindowOrientation( orientation );
};

AboutAssistant.prototype.deactivate = function(event) {
  if(isTouchpad())
    Mojo.Event.stopListening(this.controller.get('back'),
        Mojo.Event.tap,
        this.backClick_bind);
};

AboutAssistant.prototype.cleanup = function(event) {
  /* this function should do any cleanup needed before the scene is destroyed as 
     a result of being popped off the scene stack */
};

AboutAssistant.prototype.backClick = function(event) {
  //closeScene();
  this.controller.stageController.popScene();
};
