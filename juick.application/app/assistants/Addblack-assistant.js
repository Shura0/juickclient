
function AddblackAssistant(p) {
  /* this is the creator function for your scene assistant object. It will be passed all the 
     additional parameters (after the scene name) that were passed to pushScene. The reference
     to the scene controller (this.controller) has not be established yet, so any initialization
     that needs the scene controller should be done in the setup function below. */
  this.state=p.state;
}



AddblackAssistant.prototype.setup = function() {
  /* this function is for setup tasks that have to happen when the scene is first created */
    
  /* use Mojo.View.render to render view templates and add them to the scene, if needed */
  
  /* setup widgets here */
  this.blacklist=new Array();
  this.addeditems="";
  this.controller.setupWidget('text', {}, {});
  this.controller.setupWidget("add",
		this.attributes = {
		},
		this.model = {
      label : "Добавить",
      disabled: false
		}
	);
  /* add event handlers to listen to events from widgets */
  if(isTouchpad())
  {
    this.controller.get("back").style.display="block";
    this.controller.setupWidget("back",
      this.attributes = {
      },
      this.model = {
        label : "Назад",
        disabled: false
      }
    );
  this.controller.get("back").style.display="block";
  this.backClick_bind=this.backClick.bindAsEventListener(this);
  }
   
	this.addClick_bind=this.addClick.bindAsEventListener(this);
};

AddblackAssistant.prototype.activate = function(event) {
  /* put in event handlers here that should only be in effect when this scene is active. For
     example, key handlers that are observing the document */
  if(isTouchpad())
    Mojo.Event.listen(this.controller.get("back"),Mojo.Event.tap, this.backClick_bind,true);   
	Mojo.Event.listen(this.controller.get("add"),Mojo.Event.tap, this.addClick_bind,true);   
  Mojo.Log.info("State="+this.state);
  if(this.state == 'do-tags')
    this.controller.get('title').update("Введите тег");
  else
    this.controller.get('title').update("Введите имя");
};

AddblackAssistant.prototype.orientationChanged = function(orientation){
  Mojo.Log.info("Orientation Changed", orientation);
  this.controller.stageController.setWindowOrientation( orientation );
}
AddblackAssistant.prototype.deactivate = function(event) {
  /* remove any event handlers you added in activate and do any other cleanup that should happen before
     this scene is popped or another scene is pushed on top */
  if(isTouchpad())
    Mojo.Event.stopListening(this.controller.get('back'),
        Mojo.Event.tap,
        this.backClick_bind);
  Mojo.Event.stopListening(this.controller.get('add'),
        Mojo.Event.tap,
        this.addClick_bind);
  for(var i=0;i<this.blacklist.length;i++)
  {
    var a=new Object();
    a.value=this.blacklist[i];
    if(this.state == 'do-tags')
      Settings.blacklist_tag.push(a);
    else
      Settings.blacklist_author.push(a);
    Mojo.Log.info("Tag "+a.value+" added to black list");
  }
  saveSettings();
}

AddblackAssistant.prototype.cleanup = function(event) {
  /* this function should do any cleanup needed before the scene is destroyed as 
     a result of being popped off the scene stack */
};

AddblackAssistant.prototype.backClick = function(event) {
  Mojo.Controller.stageController.popScene();
  //closeScene();
}
AddblackAssistant.prototype.addClick = function(event) {
  if(this.addeditems=="")
    this.addeditems+=this.controller.get("text").mojo.getValue();
  else
    this.addeditems+=", "+this.controller.get("text").mojo.getValue();
	this.controller.get("test").update(this.addeditems);
  this.blacklist.push(this.controller.get("text").mojo.getValue());
  this.controller.get("text").mojo.setValue("");
};
