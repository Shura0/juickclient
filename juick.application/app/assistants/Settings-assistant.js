
function SettingsAssistant() {
  /* this is the creator function for your scene assistant object. It will be passed all the 
     additional parameters (after the scene name) that were passed to pushScene. The reference
     to the scene controller (this.controller) has not be established yet, so any initialization
     that needs the scene controller should be done in the setup function below. */
}

SettingsAssistant.prototype.setup = function() {
  /* this function is for setup tasks that have to happen when the scene is first created */
    
  /* use Mojo.View.render to render view templates and add them to the scene, if needed */
  
  /* setup widgets here */
  
  this.sliderModel={
    value:1,
    disabled:false
    }
  this.checkModel={
    value:true,
    disabled:false
  }
  this.gpsModel={
    value:true,
    disabled:false
  }
  this.arabModel={
    value:false,
    disabled:false
  }
    this.controller.setupWidget("checkId",
    this.attributes = {
      trueValue: 1,
      falseValue: 0
  },
  this.checkModel); 
  this.controller.setupWidget("gps",
    this.attributes = {
      trueValue: 1,
      falseValue: 0
  },this.gpsModel); 
  this.controller.setupWidget("placesList",
		this.attributes = {
		},
		this.model = {
      label : "Места",
      disabled: false
		}
	);
  this.controller.setupWidget("islider",
  this.attributes = {
      minValue: 0,
      maxValue: 2,
      round:true
  },
  this.sliderModel); 
	this.controller.setupWidget("black",
		this.attributes = {
		},
		this.model = {
      label : "Чёрный список",
      disabled: false
		}
	);
    this.controller.setupWidget("arabCheckBox",
    this.attributes = {
      trueValue: 1,
      falseValue: 0
		},this.arabModel); 
  /* add event handlers to listen to events from widgets */
  if(isTouchpad())
  {
    this.controller.get("back").style.display="block";
    this.controller.setupWidget("back",
      this.attributes = {
      },
      this.model = {
        label : "Назад",
        disabled: false
      }
    );
  this.controller.get("back").style.display="block";
  this.backClick_bind=this.backClick.bindAsEventListener(this);
  }
   
  this.checkClickHandler_bind=this.checkClickHandler.bindAsEventListener(this);
  this.gpsClickHandler_bind=this.gpsClickHandler.bindAsEventListener(this);
  this.arabClickHandler_bind=this.arabClickHandler.bindAsEventListener(this);
  this.sliderChange_bind=this.sliderChangeHandler.bindAsEventListener(this);
	this.blackClick_bind=this.blackClick.bindAsEventListener(this);
  this.placeListClick_bind=this.placeListClick.bindAsEventListener(this);
};

SettingsAssistant.prototype.activate = function(event) {
  /* put in event handlers here that should only be in effect when this scene is active. For
     example, key handlers that are observing the document */
  
  Mojo.Event.listen(this.controller.get('checkId'),
        Mojo.Event.propertyChange,
        this.checkClickHandler_bind,true);
  Mojo.Event.listen(this.controller.get('gps'),
        Mojo.Event.propertyChange,
        this.gpsClickHandler_bind,true);
  Mojo.Event.listen(this.controller.get('arabCheckBox'),
        Mojo.Event.propertyChange,
        this.arabClickHandler_bind,true);
    Mojo.Event.listen(this.controller.get('islider'),
        Mojo.Event.propertyChange ,
        this.sliderChange_bind,true);
  if(isTouchpad())
    Mojo.Event.listen(this.controller.get("back"),Mojo.Event.tap, this.backClick_bind,true);   
  
  
  this.sliderModel.value=Settings.images;
  var a=this.controller.get("idescr");
  switch(Settings.images)
  {
    case 0:
      a.update("Нет");
    break;
    case 1:
      a.update("Низкое качество");
    break;
    case 2:
      a.update("Высокое качество");
    break;
    default:break;
  }
  this.checkModel.value=Settings.websocket;
  this.gpsModel.value=Settings.use_gps;
  this.arabModel.value=Settings.block_arab;
  this.controller.modelChanged(this.sliderModel);
  this.controller.modelChanged(this.checkModel);
  this.controller.modelChanged(this.gpsModel);
  this.controller.modelChanged(this.arabModel);
	Mojo.Event.listen(this.controller.get("black"),Mojo.Event.tap, this.blackClick_bind,true);   
  Mojo.Event.listen(this.controller.get("placesList"),Mojo.Event.tap, this.placeListClick_bind,true);   
};

SettingsAssistant.prototype.orientationChanged = function(orientation){
  Mojo.Log.info("Orientation Changed", orientation);
  this.controller.stageController.setWindowOrientation( orientation );
}
SettingsAssistant.prototype.deactivate = function(event) {
  /* remove any event handlers you added in activate and do any other cleanup that should happen before
     this scene is popped or another scene is pushed on top */
  Mojo.Event.stopListening(this.controller.get('checkId'),
        Mojo.Event.propertyChange,
        this.checkClickHandler_bind,true);
  Mojo.Event.stopListening(this.controller.get('gps'),
        Mojo.Event.propertyChange,
        this.gpsClickHandler_bind,true);
  Mojo.Event.stopListening(this.controller.get('placesList'),
        Mojo.Event.tap,
        this.placeListClick_bind);
  Mojo.Event.stopListening(this.controller.get('islider'),
        Mojo.Event.propertyChange ,
        this.sliderChange_bind,true);
  if(isTouchpad())
    Mojo.Event.stopListening(this.controller.get('back'),
        Mojo.Event.tap,
        this.backClick_bind);
  Mojo.Event.stopListening(this.controller.get('black'),
        Mojo.Event.tap,
        this.blackClick_bind);
  saveSettings();
  if(Settings.websocket)
  {
    if(Settings.bookmarks)
    {
      for(var i=0;i<Settings.bookmarks.length;i++)
      {
        if(wsthread[Settings.bookmarks[i].thread]) continue;
        Mojo.Log.info("creating wsockets: "+i+"   "+Settings.bookmarks[i].thread);
        WebSocketTest(Settings.bookmarks[i].thread,"connect");
      }
    }
  }
  else
  {
    for(var i=0;i<Settings.bookmarks.length;i++)
    {
      WebSocketTest(Settings.bookmarks[i].thread,"disconnect");
    }
  }
};

SettingsAssistant.prototype.cleanup = function(event) {
  /* this function should do any cleanup needed before the scene is destroyed as 
     a result of being popped off the scene stack */
};

SettingsAssistant.prototype.checkClickHandler = function(event){
  var c=this.controller.get("checkId");
  Mojo.Log.info("checkbox clicked "+event.value);
  Settings.websocket=event.value;
}

SettingsAssistant.prototype.gpsClickHandler = function(event){
  var c=this.controller.get("gps");
  Mojo.Log.info("checkbox clicked "+event.value);
  Settings.use_gps=event.value;
  if(!event.value)
    Location_id=0;
}


SettingsAssistant.prototype.arabClickHandler = function(event){
  var c=this.controller.get("arabCheckBox");
  Mojo.Log.info("arab checkbox clicked "+event.value);
  Settings.block_arab=event.value;
}

SettingsAssistant.prototype.sliderChangeHandler = function(event){
  var a=this.controller.get("idescr");
  switch(event.value)
  {
    case 0:
      a.update("Нет");
    break;
    case 1:
      a.update("Низкое качество");
    break;
    case 2:
      a.update("Высокое качество");
    break;
    default:break;
  }
  Settings.images=event.value;
  setTimeout(function() {
                this.sliderModel.value = Settings.images;
                this.controller.modelChanged(this.sliderModel);
            }.bind(this), 50);

}
SettingsAssistant.prototype.backClick = function(event) {
  Mojo.Controller.stageController.popScene();
  //closeScene();
}
SettingsAssistant.prototype.placeListClick = function(event) {
	Mojo.Controller.stageController.pushScene("Places");
};

SettingsAssistant.prototype.blackClick = function(event) {
	Mojo.Controller.stageController.pushScene("Blacklist");
};
