function UserAssistant(p) {
  /* this is the creator function for your scene assistant object. It will be passed all the 
     additional parameters (after the scene name) that were passed to pushScene. The reference
     to the scene controller (this.controller) has not be established yet, so any initialization
     that needs the scene controller should be done in the setup function below. */
     this.userid=0;
     this.username="";
     this.more=0;
     this.lastthread=0;
     if(p)
     {
       this.username=p.username;
       this.userid=p.userid;
     }
}

UserAssistant.prototype.setup = function() {
  /* this function is for setup tasks that have to happen when the scene is first created */
    
  /* use Mojo.View.render to render view templates and add them to the scene, if needed */
  
  /* setup widgets here */
  this.controller.setupWidget("mainlistid",
        this.attributes = {
            itemTemplate: "list/listitem",
            listTemplate: "list/listcontainer",
            addItemLabel: "Ещё ...",
            swipeToDelete: false,
            reorderable: false,
      fixedHeightItems:false,
      renderLimit: 2000,
            emptyTemplate:"list/emptylist"
         },
         this.model = {
             listTitle: "",
             items : [
                 //{data: "Loading...", definition: "definition"},
             ]
          }
    );
  this.controller.setupWidget("spinnerid",
        this.attributes = {
            spinnerSize: "small"
        },
        this.model = {
            spinning: true,
        }
    );
    this.menumodel={
        visible: true,
        items: [ 
            //{ icon: "attach", command: "do-Attach"},
            { icon: '', label: "Ss", command: "do-Send"}
        ]
    }
    if(this.username.toLowerCase()==Settings.login.toLowerCase())
    {
      this.menumodel.visible=false;
    }
    this.controller.setupWidget(Mojo.Menu.commandMenu,
    this.attributes = {
        spacerHeight: 0,
        menuClass: 'no-fade'
    },this.menumodel );
  if(isTouchpad())
  {
    this.controller.get("back").style.display="block";
    this.controller.setupWidget("back",
      this.attributes = {
      },
      this.model = {
        label : "Назад",
        disabled: false
      }
    );
    this.backClick_bind=this.backClick.bindAsEventListener(this);
  }
  this.listClickHandler_bind=this.listClickHandler.bind(this);
  this.listMoreClick_bind=this.listMoreClick.bind(this);
  /* add event handlers to listen to events from widgets */
};

UserAssistant.prototype.activate = function(event) {
  
    var hh=this.controller.get("nameid");
    hh.update(this.username);
    var ava=this.controller.get("avatarid");
    Mojo.Log.info('image=http://i.juick.com/as/'+this.userid+'.png');
    ava.style.backgroundImage="url('http://i.juick.com/as/"+this.userid+".png')";
    ava.update();
    
    var np="";
    getData(this);
    Mojo.Event.listen(this.controller.get('mainlistid'),
        Mojo.Event.listTap,
        this.listClickHandler_bind,true);
        Mojo.Event.listen(this.controller.get('mainlistid'),
    Mojo.Event.listAdd,
    this.listMoreClick_bind,true);
  if(isTouchpad())
  {
    ava.style.marginLeft="128px";
    Mojo.Event.listen(this.controller.get("back"),Mojo.Event.tap, this.backClick_bind,true);   
  }
};

UserAssistant.prototype.deactivate = function(event) {
  /* remove any event handlers you added in activate and do any other cleanup that should happen before
     this scene is popped or another scene is pushed on top */
  Mojo.Event.stopListening(this.controller.get('mainlistid'),
        Mojo.Event.listTap,
        this.listClickHandler_bind);
        Mojo.Event.stopListening(this.controller.get('mainlistid'),
    Mojo.Event.listAdd,
    this.listMoreClick_bind);
  if(isTouchpad())
    Mojo.Event.stopListening(this.controller.get('back'),
        Mojo.Event.tap,
        this.backClick_bind);
};

UserAssistant.prototype.cleanup = function(event) {
  /* this function should do any cleanup needed before the scene is destroyed as 
     a result of being popped off the scene stack */
};

UserAssistant.prototype.listClickHandler = function(event){ //going to postscene
  //Mojo.Log.info("Nick:",event.item.user.uname);
  var str=""+event.originalEvent.target;
  Mojo.Log.info("tap on "+ str);
  if(str.match(/^http/))
  {
    Mojo.Log.info("returning");
    return;
  }
  else if(str.match(/HTMLImageElement/))
  {
    var src=event.originalEvent.target.src;
    if(src.match(/^file:/))return; // local file, impossible to show.
    if(isTouchpad())
    {
      src=src.replace("/photos-1024/","/p/");
      src=src.replace("/photos-512/","/p/");
    }
    else
    {
      src=src.replace("/ps/","/photos-1024/");
    }
    Mojo.Log.info("image is "+ src);
    this.controller.serviceRequest("palm://com.palm.applicationManager", {
    method: "launch",
    parameters: {
        id: 'com.palm.app.browser',
        params: {
            "target": src
        }
      }});   
    Mojo.Log.info("returning");
    return;
  }
  else if(str.match(/^file:/)) //our internal links
  {
    var index=str.indexOf("#username:");
    if (index > 0) {
      var name = str.substr(index + 11); //11- length of #username:@
      Mojo.Log.info("username:", name);
      return;
    }
    index=str.indexOf("#thread:");
    if(index>0){
      var thread=str.substr(index+11); //11 - length of #thread:%23
      Mojo.Log.info("thread #"+thread);
      Mojo.Controller.stageController.pushScene("Post", {username:"",
                                userid:0,
                                thread:thread});
      return;
    }
  }
  Mojo.Controller.stageController.pushScene("Post", {username:event.item.user.uname,
                            thread:event.item.mid,
                            userid:event.item.user.uid});
}

UserAssistant.prototype.listMoreClick = function(event){
  this.more=1;
  com = new Object();
  com.command = "do-Refresh";
  com.type = "mojo-command";
  StageAssistant.prototype.handleCommand(com);
}

UserAssistant.prototype.backClick = function(event) {
  Mojo.Controller.stageController.popScene();
  //closeScene();
};

