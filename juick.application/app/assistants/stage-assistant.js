
var newscene = {
	attach:"",
	attachtype:"img",
	thread:0,
	user:"",
	userid:0,
	replyid:0,
	uploadrequest:null,
	commandMenuModel:{
		visible: true,
        items: [
            { icon: "attach", command: "do-Attach"},
            { icon: 'send', command: "do-Send"}
        ]
	}
}

var Settings = {
  login:"",
  password:"",
  bookmarks:[],
	blacklist_tag:[],
	blacklist_author:[],
  places:[],
  use_gps:0,
	block_arab:0,
  userid:-1,
  websocket:true,
  images:1 //0 - no images, 1 - low res, 2 - normal size

}
var Location_id=0;

var wifiConnection=0;

function loadSettings(){
    //old version compatibility
    var a = new Mojo.Model.Cookie("s");
    if(a.get())
    {
      Settings.login=a.get();
      //a.remove();
    }
    var a=new Mojo.Model.Cookie("p");
    if(a.get())
    {
      Settings.password=a.get();
      //a.remove();
    }
    var a=new Mojo.Model.Cookie("a");
    if(a.get())
    {
      Settings.bookmarks=a.get();
      //a.remove();
    }
    //new version
    var j=new Mojo.Model.Cookie("juick");
    if(j.get())
    {
      Mojo.Log.info("new settings format found!");
      Settings=JSON.parse(j.get());
      if(!Settings.blacklist_tag)
        Settings.blacklist_tag=new Array();
      if(!Settings.blacklist_author)
        Settings.blacklist_author=new Array();
      if(!Settings.places)
        Settings.places=new Array();
    }
    //Settings.blacklist_tag=[];
    Mojo.Log.info("Settings loaded");

}

function saveSettings() {
  var text=JSON.stringify(Settings);
  var j=new Mojo.Model.Cookie("juick");
  j.put(text);
  Mojo.Log.info("Settings saved");
  Mojo.Log.info("Settings are:",text);
}

function isTouchpad()
{
  var model=Mojo.Environment.DeviceInfo.modelNameAscii;
  Mojo.Log.info("Model is: "+model);
  if(model.indexOf("ouch")>-1)
  {
    Mojo.Log.info("Checking for touchpad...true");
    return true;
  }
  if(Mojo.Environment.DeviceInfo.screenWidth==1024 || Mojo.Environment.DeviceInfo.screenHeight==1024)
  {
    Mojo.Log.info("Checking for touchpad...true");
    return true;
  }
  Mojo.Log.info("Checking for touchpad...false");
  return false;
}

var wsthread =new Array();


//var answered=new Array();
var notifications=new Array();

//websocket handlers
var wsr=0;
var wsall=0;
var service=0;

function StageAssistant() {
	/* this is the creator function for your stage assistant object */
  //this.wsr=0;
  //this.wsall=0;
}

StageAssistant.prototype.setup = function() {
  if (!("WebSocket" in window))
  {
    service=new Mojo.Service.Request("palm://ru.ya.shura0.juickclient.service", {
      method: "connect",
      parameters: {subscribe:true },
      onSuccess: this.serviceSuccess.bind(this),
      onFailure: this.serviceFailure.bind(this)
    });
  }
	/* this function is for setup tasks that have to happen when the stage is first created */
	this.controller.pushScene("Main");
  loadSettings();
  if(!Settings.userid || Settings.userid<0)getUserId();
}

StageAssistant.prototype.serviceSuccess = function (data)
{
  //if(!Settings.websocket)return;
  Mojo.Log.info("Connected to service!");
  Mojo.Log.info("Type=",data.reply.type,"thread=",data.reply.thread);
  if(!data.reply.thread) return; // Just subscribe
  if(data.reply.type=="all")
  {
    Mojo.Log.info("message type=",typeof(data.reply.message.data));
    Mojo.Log.info("data=",data.reply.message.data);
    var t=data.reply.message.data;
    wsall_onmessage({data:t});
  }
  else //answer in post
  {
    var t = data.reply.message.data;
    Mojo.Log.info("Reply to thread "+data.reply.thread+" is received...");

    var controller=Mojo.Controller.stageController.activeScene();
    var list=controller.get('mainlistid');
    var p=controller.assistant;
    var myData=prepare_message(t);
    var uname=Settings.login;
    //Checking if it my own message
    if(myData.user.uname.toLowerCase()==uname.toLowerCase())return;
    if(controller.sceneName=="Post" && p.thread == data.reply.thread)
    {
      var l = list.mojo.getLength();
      var a=new Array();
      a.push(myData);
      Mojo.Log.info("list length=", l);
      list.mojo.noticeAddedItems(l,a);
    }
    else // add notification
    {
      var appController = Mojo.Controller.getAppController();
      var m={
        type:"reply",
        message:myData
      }
      notifications.push(m);
      var message=m;
      Mojo.Log.info("Added to nots: "+JSON.stringify(m));
      this.dashboardcount = this.dashboardcount+1;
      var count = this.dashboardcount;
      var dashboardStage = appController.getStageProxy("myDashboard");
      if(dashboardStage)
      {
        dashboardStage.delegateToSceneAssistant("updateDashboard", message, count);
      }
      else
      {
        this.dashboardcount=1;
        count = this.dashboardcount;
        var pushDashboard = function(stageController){
            stageController.pushScene('Dashboard', message, count);
        };
        appController.createStageWithCallback({name: "myDashboard", lightweight: true},
          pushDashboard, 'Dashboard');
      }
    }
  }
}

StageAssistant.prototype.serviceFailure = function(data)
{
  Mojo.Log.error("Cannot subscribe on service");
}

addBookmark = function(name, thread)
{
	for(i=0;i<Settings.bookmarks.length;i++) //ignore dups
		if(Settings.bookmarks[i].thread==thread)
			return;
  Mojo.Log.info("Bookmark added:"+name+":"+thread);
  var i=Settings.bookmarks.length;
  Settings.bookmarks[i]={};
  Settings.bookmarks[i].thread=thread;
	Settings.bookmarks[i].name=name;
  saveSettings();
  WebSocketTest(thread, "connect");
}

function getAuth()
{
	Mojo.Log.info("getAuth()");
	this.controller=Mojo.Controller.stageController.activeScene();
	this.controller.showDialog({
		template: "Auth-request/Auth-request-scene",
		assistant: new CustomDialogAssistant(this)
	});
	Mojo.Log.info("Dialog closed");

}

function newPlace()
{
	Mojo.Log.info("addPlace()");
	this.controller=Mojo.Controller.stageController.activeScene();
	this.controller.showDialog({
		template: "Place-request/Place-request-scene",
		assistant: new PlaceDialogAssistant(this)
	});
	Mojo.Log.info("Dialog closed");
}


function checkAuth()
{
	if (Settings.login && Settings.password)
		return true;
	else return false;
}

function getUserId()
{
  if(!Settings.login)return;
  var xmlhttp = new XMLHttpRequest();
  Mojo.Log.info("open connection");
  var request = 'http://api.juick.com/users?uname='+Settings.login;
  xmlhttp.open('GET', request, true);
  Mojo.Log.info("Request="+request);
  xmlhttp.onreadystatechange = function(){
		if (xmlhttp.readyState == 4) {
			Mojo.Log.info("readystate=4");
			if (xmlhttp.status == 200) {
				Mojo.Log.info("status=200");
				var t=xmlhttp.responseText;
        Mojo.Log.info("Received text="+t);
        var o=JSON.parse(t);
        Settings.userid=o[0].uid;
        Mojo.Log.info("UserID="+Settings.userid);
        saveSettings();
      }
      else {
        switch(xmlhttp.status)
        {
          case 0:
            Mojo.Controller.errorDialog("Невозможно подключиться к сервру");
          break;
          case 403:
            Mojo.Controller.errorDialog("Forbidden<br>Неверный логин или пароль"+xmlhttp.statusText);
          break;
          case 404:
            Mojo.Controller.errorDialog("Not found<br>Запись не найдена, возможно она была удалена пользователем"+xmlhttp.statusText);
          break;
          default:
            Mojo.Controller.errorDialog(xmlhttp.status+"<br>"+xmlhttp.statusText);
          break;
        }
        //if(spin)
        //{
//          spin.mojo.stop();
          //spin.style.visible = "none";
        //}
      }
    }
  }
  xmlhttp.send();
}
/*
function addPlace(name, a)
{
  if(!Settings.login)return;
  var xmlhttp = new XMLHttpRequest();
  var np="";
  if(Settings.login && Settings.password)
  {
    np=Settings.login+":"+Settings.password;
  }
  else{
    getAuth();
    break;
  }
  var auth=Base64.encode(np);
  Mojo.Log.info("open connection");
  var request = 'http://api.juick.com/place_add?name='+name+"&lat="+lat+"&lon="+lon;
  xmlhttp.open('GET', request, true);
  xmlhttp.setRequestHeader("Authorization", "Basic " + auth);
  Mojo.Log.info("Request="+request);
  xmlhttp.onreadystatechange = function(){
		if (xmlhttp.readyState == 4) {
			Mojo.Log.info("readystate=4");
			if (xmlhttp.status == 200) {
				Mojo.Log.info("status=200");
				var t=xmlhttp.responseText;
        Mojo.Log.info("Received text="+t);
        var o=JSON.parse(t);
        Settings.places=o[0].pid;
        a.pid=o[0].pid;
        Settings.places[name]=a;
        //Mojo.Log.info("UserID="+Settings.userid);
        saveSettings();
      }
      else {
        switch(xmlhttp.status)
        {
          case 0:
            Mojo.Controller.errorDialog("Невозможно подключиться к сервру");
          break;
          case 403:
            Mojo.Controller.errorDialog("Forbidden<br>Неверный логин или пароль"+xmlhttp.statusText);
          break;
          case 404:
            //If this is new place then server returns error 404. All right.
          break;
          default:
            Mojo.Controller.errorDialog(xmlhttp.status+"<br>"+xmlhttp.statusText);
          break;
        }
      }
    }
  }
  xmlhttp.send();
}
*/
function parser(key, value){
	if (key == "body") {
		//links
    if(matches=value.match(/(\s|^)(https?:\/\/(?:www\.)?([^\/\s\n\"]+)\/?[^\s\n\"]*)/g))
    {
      Mojo.Log.info("Found links:",matches[0]);
      if(matches[0].match(/(\.jpg|\.png|\.jpeg)$/i))
      {
        value=value.replace(/(\s|^)(https?:\/\/(?:www\.)?([^\/\s\n\"]+)\/?[^\s\n\"]*)/g,"$1<a href=\"$2\"><img src='images/mime-image.png'/> $3</a>");
      }
      else if(matches[0].match(/(\.avi|\.mpg|\.mpeg|\.mov|\.flv|\.mp4)$/i))
      {
        value=value.replace(/(\s|^)(https?:\/\/(?:www\.)?([^\/\s\n\"]+)\/?[^\s\n\"]*)/g,"$1<a href=\"$2\"><img src='images/mime-video.png'/> $3</a>");
      }
      else if(matches[0].match(/(\.mp3|\.wav|\.ape|\.ogg)$/i))
      {
        value=value.replace(/(\s|^)(https?:\/\/(?:www\.)?([^\/\s\n\"]+)\/?[^\s\n\"]*)/g,"$1<a href=\"$2\"><img src='images/mime-music.png'/> $3</a>");
      }
      else if(matches[0].match(/(\.pdf)$/i))
      {
        value=value.replace(/(\s|^)(https?:\/\/(?:www\.)?([^\/\s\n\"]+)\/?[^\s\n\"]*)/g,"$1<a href=\"$2\"><img src='images/mime-pdf.png'/> $3</a>");
      }
      else
      {
        value=value.replace(/(\s|^)(https?:\/\/(?:www\.)?([^\/\s\n\"]+)\/?[^\s\n\"]*)/g,"$1<a href=\"$2\">$3</a>");
      }
    }
		// @username
		value=value.replace(/(\s|^)(@[\w-@]+)/g,"$1<a href=#username:$2>$2</a>");
		// #thread
		value=value.replace(/(\s|^)(#\d{1,7}\/?\d{1,3})/g,"$1<a href=#thread:$2>$2</a>");
		// *Bold*
		value=value.replace(/(\s|^)\*([^\*\n<>]+)\*(\s|$)/g,"$1<b>$2</b>$3");
		// /italic/
		value=value.replace(/(\s|^)\/([^\/\n<>]+)\/(\s|$)/g,"$1<i>$2</i>$3");
		// _underline_
		value=value.replace(/(\s|^)\_([^\_\n<>]+)\_(\s|$)/g,"$1<u>$2</u>$3");
		// ~strike~
    value=value.replace(/(\s|^)\~([^\~\n<>]+)\~(\s|$)/g,"$1<s>$2</s>$3");

    //quote
    value=value.replace(/^&gt; (.*)/gm,"<i class='quote'>$1</i>");

		value=value.replace(/\\n/g,"<br>");
		value=value.replace(/\n/g,"<br>");

		//Mobile sites.
		//lenta.ru
		value=value.replace(/\<a href=\"http:\/\/lenta.ru\//g,"<a href=\"http://pda.lenta.ru/");
		//livejournal
		value=value.replace(/\<a href=\"http:\/\/([^\.]+)\.livejournal\.com\/(\d+)\.html/g,"<a href=\"http://m.livejournal.com/read/user/$1/$2");

	}else if(key=="tags") {

		value=""+value;
		value=value.replace(/,/g," ");
	}
	return value;

}


function remove_blacklisted(messages)
{
	var whitemessages=new Array();
	var flag;
  Mojo.Log.info("messages.length=",messages.length);
  if(!messages.length) return messages;
	for(var i=0;i<messages.length;i++)
	{
		flag=0;
		for(var j=0;j<Settings.blacklist_tag.length;j++)
		{
      if(!messages[i].tags) break;
      var tags=messages[i].tags.split(" ");
      if(!Settings.blacklist_tag[j].value || Settings.blacklist_tag[j].value=='')continue;
			for(var a=0;a<tags.length;a++)
			{
				if(tags[a].toLowerCase()==Settings.blacklist_tag[j].value.toLowerCase())
				{
					flag=1;
					Mojo.Log.info("Removed message "+messages[i].mid+" beacuse tag "+Settings.blacklist_tag[j].value);
					break;
				}
				if(flag)break;
			}
			if(flag)break;
		}
		for(var j=0;j<Settings.blacklist_author.length;j++)
		{
			if(messages[i].user.uname.toLowerCase() == Settings.blacklist_author[j].value.toLowerCase())
			{
				flag=1;
				Mojo.Log.info("Removed message "+messages[i].mid+" beacuse name "+Settings.blacklist_author[j].value);
				break;
			}
		}
		if(!flag)
		{
			whitemessages.push(messages[i]);
		}
	}
	return whitemessages;
}


remove_arab=function(messages)
{
  var mes=new Array();
  if(!messages.length)return messages;
  for(var i=0;i<messages.length;i++)
  {
    var flag=0;
    for(var j=0;j<25;j++)
    {
      var ch=messages[i].body.charCodeAt(j)
      if(ch === NaN)
        break;
      if((ch > 0x0600 && ch< 0x0700) || (ch > 0x0750 && ch < 0x0780) ||(ch>0xfb50 && ch <0xfe00) || (ch > 0xfe70 && ch < 0xff00))
      {
        flag=1;
        Mojo.Log.info("Removed message "+messages[i].mid+" beacuse it's arabian shit");
        break;
      }
    }
    if(!flag)
      mes.push(messages[i]);
  }
  return mes;
}

prepare_message=function(message)
{
  message=message.replace(/\</g,"&lt;");
  message=message.replace(/\>/g,"&gt;");
  message=message.replace(/\t/g," ");
  var myData=JSON.parse(message,parser);
  Mojo.Log.info(" DATA:",JSON.stringify(myData));
  if(Settings.bookmarks)
  {
    for (var j=0;j<Settings.bookmarks.length;j++)
    {
      for(var i=0;i<myData.length;i++)
      {
        if(Settings.bookmarks[j].thread==myData[i].mid && (! myData[i].rid) )
        {
          myData[i].styleclass="withme"; //mark bookmark messages
        }
      }
    }
  }
  //Date routine
  var timezoneoffset=new Date().getTimezoneOffset()*60000;
  var uname=Settings.login;
	myData=remove_blacklisted(myData);
  if(Settings.block_arab)
    myData=remove_arab(myData);
  for(var i=0;i<myData.length;i++)
  {
    var date=new Date(myData[i].timestamp);
    date.setTime(date.getTime()-timezoneoffset*2);
    var d=new Array();
    d[0]=date.getUTCHours();
    d[1]=date.getMinutes();
    d[2]=date.getDate();
    d[3]=date.getMonth()+1;
    for(j=0;j<d.length;j++)
    {
      if(d[j]<10)d[j]="0"+d[j];
    }
    myData[i].mytimestamp=d[0]+":"+d[1]+" "+d[2]+"."+d[3];
    if(myData[i].user.uname.toLowerCase()==uname.toLowerCase())
    {
      myData[i].styleclass="myself"; //mark myself messages
    }
    if(myData[i].photo)
    {
      Mojo.Log.info("Photo found! "+JSON.stringify(myData[i].photo));
      if(isTouchpad())
      {
        myData[i].imagelink="<img style='max-width:100%' src="+
        myData[i].photo.medium+">";
      }
      else
      {
        if(wifiConnection!="connected")
        {
          if(Settings.images==0)
          {
            myData[i].imagelink="<a href="+myData[i].photo.medium+">[IMAGE]</a></br>";
          }
          else if(Settings.images==1)
          {
            if(myData[i].photo.thumbnail)
              myData[i].imagelink="<img width=100% src="+myData[i].photo.thumbnail+">";
            else
              myData[i].imagelink="<img width=100% src="+myData[i].photo.small+">";
          }
          else if(Settings.images==2)
          {
            myData[i].imagelink="<img width=100% src="+myData[i].photo.medium+">";
          }
        }
        else
        {
          myData[i].imagelink="<img width=100% src="+myData[i].photo.medium+">";
        }
      }
    }
    if(myData[i].video)
    {
      myData[i].videolink="<a href="+myData[i].video.mp4+"><img src='images/video.png'></a></br>";
    }
  }
  return myData;
}

wsall_onmessage = function (evt)
{
  //var t = JSON.stringify(evt.data);
  var t=evt.data;
  Mojo.Log.info("Message is received...");
  Mojo.Log.info(t);
  var controller=Mojo.Controller.stageController.activeScene();
  var list=controller.get('mainlistid');
  var p=controller.assistant;
  var myData=prepare_message(t);
  if(p.list == "last")
  {
    var l = list.mojo.getLength();
    Mojo.Log.info("list length=", l);
    var arr=new Array();
    p.messages = arr.concat(myData,p.messages);
    Mojo.Log.info("messages length=", p.messages.length);
    list.mojo.setLength(0);
    list.mojo.noticeUpdatedItems(0,p.messages);
  }
  else // add notification
  {
    var appController = Mojo.Controller.getAppController();
    //var message=myData;
    var m={
      type:"all",
      message:myData}
    notifications.push(m);
    var message=m;
    Mojo.Log.info("Added to nots: "+JSON.stringify(m));
    this.dashboardcount = this.dashboardcount+1;
    var count = this.dashboardcount;
    var dashboardStage = appController.getStageProxy("myDashboard");
    if(dashboardStage) {
      dashboardStage.delegateToSceneAssistant("updateDashboard", message, count);
    } else {
      this.dashboardcount=1;
      count = this.dashboardcount;
      var pushDashboard = function(stageController){
          stageController.pushScene('Dashboard', message, count);
      };
      appController.createStageWithCallback({name: "myDashboard", lightweight: true},
        pushDashboard, 'Dashboard');
    };
  }
}

function WebSocketTest(type, action)
{
  if(!Settings.websocket)return;
  if ("WebSocket" in window)
  {
    Mojo.Log.info("WebSocket routine. Action=",action,"type=",type);
    if(action=="connect")
    {
      if(!wsthread[type])
      {
        var o={
          thread:type,
          socket:new WebSocket("ws://ws.juick.com/"+type),
        };
        (function(o) {
          o.socket.onopen= function(){
            var handshake = "GET " + "/" + " HTTP/1.1\r\n" +
            "Host: " + "ws.juick.com" + "\r\n"+
            "Connection: Upgrade\r\n" +
            "Upgrade: WebSocket\r\n" +
            "Origin: http://juick.com/\r\n" +
            "Sec-WebSocket-Key1: 9 9 9 9\r\n" +
            "Sec-WebSocket-Key2: 8 8 8 8 8\r\n" +
            "Sec-WebSocket-Protocol: sample\r\n" +
            "Sec-WebSocket-Key: AQIDBAUGBwgJCgsMDQ4PEC==\r\n"+
            "Sec-WebSocket-Version: 13\r\n"+
            "\r\n" + "12345678";
            // Web Socket is connected, send data using send()
            Mojo.Log.info(handshake);
            this.send(handshake);
            Mojo.Log.info("Subscribed to replies to post "+o.thread);
            var controller=Mojo.Controller.stageController.activeScene();
            if(controller.sceneName=="Post")
            {
              var status=controller.get('ws_status');
              status.style.display="inline";
              status.style.backgroundImage="url('images/green_point.png')";
            }
          };
          o.socket.onclose= function()
          {
            // websocket is closed.
            Mojo.Log.info("Replies to "+o.thread+" connection is closed...");
            wsthread[o.thread]=0;
            delete wsthread[o.thread];
            var controller=Mojo.Controller.stageController.activeScene();
            if(controller.sceneName=="Post")
            {
              var status=controller.get('ws_status');
              status.style.display="inline";
              status.style.backgroundImage="url('images/red_point.png')";
            }

          };
          o.socket.onmessage=function (evt)
          {
            var t=evt.data;
            Mojo.Log.info("Reply to thread "+o.thread+" is received...");
            //Mojo.Log.info(t);
            var controller=Mojo.Controller.stageController.activeScene();
            var list=controller.get('mainlistid');
            var p=controller.assistant;

            var myData=prepare_message(t);
            var uname=Settings.login;
            if(myData.user.uname.toLowerCase()==uname.toLowerCase())return;
            if(controller.sceneName=="Post" && p.thread == o.thread)
            {
              var l = list.mojo.getLength();
              var a=new Array();
              a.push(myData);
              Mojo.Log.info("list length=", l);
              list.mojo.noticeAddedItems(l,a);
            }
            else // add notification
            {
              var appController = Mojo.Controller.getAppController();
              //var message=myData;
              var m={
                type:"reply",
                message:myData}
              notifications.push(m);
              var message=m;
              Mojo.Log.info("Added to nots: "+JSON.stringify(m));
              this.dashboardcount = this.dashboardcount+1;
              var count = this.dashboardcount;
              var dashboardStage = appController.getStageProxy("myDashboard");
              if(dashboardStage) {
                dashboardStage.delegateToSceneAssistant("updateDashboard", message, count);
              } else {
                this.dashboardcount=1;
                count = this.dashboardcount;
                var pushDashboard = function(stageController){
                    stageController.pushScene('Dashboard', message, count);
                };
                appController.createStageWithCallback({name: "myDashboard", lightweight: true},
                  pushDashboard, 'Dashboard');
              }
            }
          };
          o.socket.onerror= function()
          {
            Mojo.Log.info("Error on thread "+o.thread+" is received...");
            this.close();
          }
          wsthread[type]=o;
        })(o);
      }
      else
      {
        Mojo.Log.info("Already connected. Ignored");
        Mojo.Log.info("OBJ:",JSON.stringify(wsthread[type]));
      }
    }
    else //close socket
    {
      if(type == "all")
      {
        if(wsall)wsall.close();
        wsall=0;
      }
      else
      {
        Mojo.Log.info("Obj:",JSON.stringify(wsthread[type]));
        if(wsthread[type] && wsthread[type].socket)
        {
          Mojo.Log.info("Closing socket for "+wsthread[type].thread);
          wsthread[type].socket.close();
        }
      }
    }
  }
  else //WebSocket is not supported, trying to use service
  {
    Mojo.Log.info("WebSocket NOT supported");
    Mojo.Log.info("Service routine. Action=",action,"type=",type);
    if(action == "connect")
    {
      if(!Settings.websocket)return;
      if(type=="all")
      {
        if(!wsall)
        {
          this.controller.serviceRequest("palm://ru.ya.shura0.juickclient.service", {
            method: "add",
            parameters: {thread:type},
            onSuccess:function(data){
              Mojo.Log.info("Subscribed to all messages");
                var controller=Mojo.Controller.stageController.activeScene();
                var status=controller.get('ws_status');
                status.style.backgroundImage="url('images/green_point.png')";
                status.style.display="inline";
                wsall=1;
            }, //this.serviceSuccess.bind(this),
            onFailure:function(data){
            var controller=Mojo.Controller.stageController.activeScene();
            var status=controller.get('ws_status');
            //status.style.backgroundImage="url('images/green_point.png')";
            status.style.display="none";
              Mojo.Log.info(JSON.stringify(data))} //this.serviceFailure.bind(this)
          });
        }
        else
        {
          Mojo.Log.info("Already connected");
          this.controller.serviceRequest("palm://ru.ya.shura0.juickclient.service",{
            method: "status",
            parameters:{thread:type},
            onSuccess:function(data){


              var controller=Mojo.Controller.stageController.activeScene();
              var status=controller.get('ws_status');
                status.style.display="inline";
              switch(data.reply)
              {
                case 1: //open
                  status.style.backgroundImage="url('images/green_point.png')";
                break;
                case 2: //closing
                case 0: //connecting
                  status.style.backgroundImage="url('images/yellow_point.png')";
                break;
                case 3: //closed
                case -1:
                  status.style.backgroundImage="url('images/red_point.png')";
                break;
                  //status.style.backgroundImage="url('images/green_point.png')";
              }

            }
          });
        }
      }
      else
      {
        if(!wsthread[type])
        {
          this.controller.serviceRequest("palm://ru.ya.shura0.juickclient.service", {
            method: "add",
            parameters: {thread:type},
            onSuccess:function(data){
              Mojo.Log.info("Subscribed to replies to post "+data.reply.thread);
              wsthread[data.reply.thread]=1;
              var controller=Mojo.Controller.stageController.activeScene();
              if(controller && controller.sceneName=="Post")
              {
                var status=controller.get('ws_status');
                status.style.display="inline";
                status.style.backgroundImage="url('images/green_point.png')";
              }
            }, //this.serviceSuccess.bind(this),
            onFailure:function(data){Mojo.Log.info(JSON.stringify(data))} //this.serviceFailure.bind(this)
          });
        }
      }
    }
    else //disconnect
    {
      this.controller.serviceRequest("palm://ru.ya.shura0.juickclient.service", {
            method: "delete",
            parameters: {thread:type},
            onSuccess:function(data){
              Mojo.Log.info("Unsubscribed from replies to post "+data.reply.thread);
              var controller=Mojo.Controller.stageController.activeScene();
              if(data.reply.thread=="all")wsall=0;
              wsthread[data.reply.thread]=0;
              delete wsthread[data.reply.thread];
              if(controller && controller.sceneName=="Post")
              {
                var status=controller.get('ws_status');
                status.style.display="inline";
                status.style.backgroundImage="url('images/red_point.png')";
              }
            }, //this.serviceSuccess.bind(this),
            onFailure:function(data){Mojo.Log.info(JSON.stringify(data))} //this.serviceFailure.bind(this)
          });
    }
    //var controller=Mojo.Controller.stageController.activeScene();
    //Mojo.Log.info("WebSocket NOT supported");
    //var status=controller.get('ws_status');
    //if(status)
//      status.style.display="none";
  }
}

function getData(p)
{
	this.controller=Mojo.Controller.stageController.activeScene();
  this.controller.serviceRequest('palm://com.palm.connectionmanager', {
     method: 'getstatus',
     parameters: {},
     onSuccess : function (e){ wifiConnection=e.wifi.state; }});
	var list=this.controller.get('mainlistid');
	var spin=this.controller.get('spinnerid');
	if(spin)
	{
		spin.mojo.start();
		spin.style.visible="block";
	}
	var type=this.controller.sceneName;
	Mojo.Log.info("SceneName=",type);
	var xmlhttp = new XMLHttpRequest();
	Mojo.Log.info("open connection");
	var request="localhost";
  //Mojo.Log.info("p.list="+p.list);
	if (type == "Main") {
		if (p.list == "last") {
			if (p.more) {
				request = 'http://api.juick.com/messages?before_mid=' + p.lastthread;
				Mojo.Log.info("lastthread=", p.lastthread);
			}
			else {
				request = 'http://api.juick.com/messages?1=1';
				p.lastthread = 0;
			}
      //var status=this.controller.get('ws_status');
      //status.style.display="inline";
      //WebSocketTest("all","connect");

      //WebSocketTest("replies","connect");
		}
		else if(p.list=="home"){
			if (p.more) {
				request = 'http://api.juick.com/home?before_mid=' + p.lastthread;
				Mojo.Log.info("lastthread=", p.lastthread);
			}
			else {
				request = 'http://api.juick.com/home';
				p.lastthread = 0;
			}
      //WebSocketTest("all","disconnect");
      var status=this.controller.get('ws_status');
      status.style.display="none";
		}
	}
	else if(type=="Post"){
		request='http://api.juick.com/thread?mid='+p.thread;
		Mojo.Log.info("get thread=", p.thread);
	}
	else if(type=="User"){
		if(p.more)
			request='http://api.juick.com/messages?user_id='+p.userid+"&before_mid="+p.lastthread;
		else
			request='http://api.juick.com/messages?user_id='+p.userid;
	}
	xmlhttp.open('GET', request, true);
	Mojo.Log.info("Request="+request);

	if (checkAuth()) { //for home list
		var np=Settings.login+":"+Settings.password;
    Mojo.Log.info("account: "+np);
		var auth=Base64.encode(np);
		xmlhttp.setRequestHeader("Authorization", "Basic " + auth);
	}

	xmlhttp.onreadystatechange = function(){
		//Mojo.Log.error("state change");
		if (xmlhttp.readyState == 4) {
			Mojo.Log.info("readystate=4");
			if (xmlhttp.status == 200) {
				Mojo.Log.info("status=200");
				var t=xmlhttp.responseText;
				var myData;
				if (type == "Main") {
					myData=prepare_message(t);
					p.lastthread=myData[myData.length-1].mid;
					if (p.more) {
						var l = list.mojo.getLength();
						Mojo.Log.info("list length=", l);
						p.messages = p.messages.concat(myData);
						Mojo.Log.info("messages length=", p.messages.length);
						list.mojo.setLength(0);
						list.mojo.noticeUpdatedItems(0,p.messages);
						list.mojo.revealItem(l, true);
						p.more = 0;
					}
					else {
						p.messages=myData;
						list.mojo.setLength(0);
						list.mojo.noticeUpdatedItems(0, p.messages);
					}
				}
				else if(type=="Post")
				{
          myData=prepare_message(t);
					Mojo.Log.info("post list updated");
					myData[0].styleclass="firstmessage";
					list.mojo.noticeUpdatedItems(0, myData);
          if(p.reply)
          {
            //list.mojo.revealItem(p.reply, true);
            list.mojo.revealItem(list.mojo.getLength(), true);
          }
					this.controller=Mojo.Controller.stageController.activeScene();
					p.username=myData[0].user.uname;
					p.userid=myData[0].user.uid;

					if(this.controller.sceneName=="Post")
						this.controller.get('headertext').update(p.username);

				} else if(type=="User")
          {
          myData=prepare_message(t);
					p.lastthread=myData[myData.length-1].mid;
					if(p.more)
					{
						var l = list.mojo.getLength();
						Mojo.Log.info("list length=", l);
						p.messages = p.messages.concat(myData);
						Mojo.Log.info("messages length=", p.messages.length);
						list.mojo.setLength(0);
						list.mojo.noticeUpdatedItems(0,p.messages);
						list.mojo.revealItem(l, true);
						p.more=0;
					}
					else
					{
						p.messages=myData;
						list.mojo.noticeUpdatedItems(0, myData);
					}
				}
				if(spin)
				{
					spin.mojo.stop();
					spin.style.visible = "none";
				}
        //Let's connect to websocket
        if(Settings.bookmarks && Settings.websocket)
        {
          for(var i=0;i<Settings.bookmarks.length;i++)
          {
            if(wsthread[Settings.bookmarks[i].thread]) continue;
            Mojo.Log.info("creating wsockets: "+i+"   "+Settings.bookmarks[i].thread);
            WebSocketTest(Settings.bookmarks[i].thread,"connect");
          }
        }
			}
			else {
				switch(xmlhttp.status)
				{
					case 0:
						Mojo.Controller.errorDialog("Невозможно подключиться к сервру");
					break;
					case 403:
						Mojo.Controller.errorDialog("Forbidden<br>Неверный логин или пароль"+xmlhttp.statusText);
					break;
					case 404:
						Mojo.Controller.errorDialog("Not found<br>Запись не найдена, возможно она была удалена пользователем"+xmlhttp.statusText);
					break;
					default:
						Mojo.Controller.errorDialog(xmlhttp.status+"<br>"+xmlhttp.statusText);
					break;
				}
				if(spin)
				{
					spin.mojo.stop();
					spin.style.visible = "none";
				}

			}
		}
	}
	xmlhttp.send();
}

uploadSuccess=function(resp)
{
	newscene.attach="";
	Mojo.Log.info('Success : ' + Object.toJSON(resp));
	Mojo.Log.info('server returns', resp.httpCode);
	this.controller=Mojo.Controller.stageController.activeScene();
	if(this.controller.sceneName=="New")
	{
		var div=this.controller.get("imgdiv");
		var spin=this.controller.get('spinnerid');
		if(div)
		{
			div.style.display="none";
			spin.style.visible="none";
			spin.mojo.stop();
		}
		newscene.commandMenuModel.items[1].disabled=false;
		Mojo.Controller.stageController.popScene(this.controller, this);
	}
	if(resp.completed)
	{
		if(resp.httpCode==200)
		{
		Mojo.Controller.getAppController().showBanner("File uploaded",
				{source: 'notification'});
			if (newscene.thread)
			{
				addBookmark(newscene.thread);
				newscene.thread=0;
			}
		}
		else
		{
			Mojo.Controller.errorDialog(resp.httpCode)
		}
		newscene.commandMenuModel.items[1].disabled=false;
		newscene.uploadrequest.cancel();
	}
}

StageAssistant.prototype.handleCommand= function(event) {
	this.controller=Mojo.Controller.stageController.activeScene();
	var type=this.controller.sceneName;
	//Mojo.Log.info("controller=",Object.toJSON(this.controller));
	var p=this.controller.assistant;
    if (event.type == Mojo.Event.command) {
        switch (event.command) {
            case "do-New":
				Mojo.Controller.stageController.pushScene("New", {thread:0,replyid:0});
			break;
			case "do-Refresh":
				Mojo.Log.info("do-Refresh");
				if((!p.more) && p.messages && (type=="Main"))
				{

					p.messages.clear();
					this.controller.get("mainlistid").mojo.revealItem(0,true);
				}
				getData(p);
			break;
	        case "do-Send":
				//Mojo.Log.info("Send button pressed");
				var tt=this.controller.get("newtext");
				var spin=this.controller.get('spinnerid');
				spin.style.visible="block";
				spin.mojo.start();
				var np="";
				if(Settings.login && Settings.password)
				{
					np=Settings.login+":"+Settings.password;
				}
				else{
					getAuth();
					break;
				}
				var auth=Base64.encode(np);
				var end = "\r\n";
				var twoHyphens = "--";
				var boundary = "****+++++******+++++++********";
				var txt="";
				if(this.controller.sceneName=="New"){
					txt=tt.mojo.getValue();
          Mojo.Log.info("place_id=",Location_id);
					newscene.commandMenuModel.items[1].disabled=true;
					this.controller.modelChanged(newscene.commandMenuModel);
				}
				else if(this.controller.sceneName=="Post")
				{
					txt="! #"+p.thread;
					Mojo.Log.info("post "+p.thread+" recommended");
					addBookmark(p.username,p.thread);
				}
				else if(this.controller.sceneName=="User")
				{
					txt="S @"+p.username;
					Mojo.Log.info("Subscribed to @"+p.username);
				}
				if(txt.length<2)return;
				if(newscene.attach.length>0) // sending picture or video
				{
					newscene.uploadrequest = new Mojo.Service.Request('palm://com.palm.downloadmanager/', {
						method: 'upload',
						parameters:
						{
							'fileName': newscene.attach,
							'url': 'http://'+np+'@api.juick.com/post',
							'fileLabel' : 'attach',
							'contentType': newscene.attachtype,
							subscribe: true,
							postParameters: [
								{
									key:"place_id",
									data:Location_id,
									contentType:"multipart/form-data"
								},
								{
									key:"referer",
									data:"juick.com",
									contentType:"multipart/form-data",
								},
								{
									key:"body",
									data:txt,
									contentType:"multipart/form-data"
								}
								]
						},
						onSuccess : uploadSuccess.bind(this),
						onFailure : function (e){
							Mojo.Log.info('Failure : ' + Object.toJSON(e));
							spin.style.visible="none";
							spin.mojo.stop();
							Mojo.Controller.errorDialog(e.errorCode);
							newscene.commandMenuModel.items[1].disabled=false;
							this.controller=Mojo.Controller.stageController.activeScene();
							//this.controller.modelChanged(newscene.commandMenuModel);
						}.bind(this)
					});
					Mojo.Controller.getAppController().showBanner("Uploading...",{source: 'notification'});
					break;
				}
				var outStr=boundary+end;
        outStr += boundary+end+"Content-Disposition: form-data; name=\"place_id\"" + end + end + Location_id + end;
        //location test
        //outStr += "Content-Disposition: form-data; name=\"lat\"" + end + end + "55.914584" + end;
        //outStr += twoHyphens + boundary + end;
        //outStr += "Content-Disposition: form-data; name=\"lon\"" + end + end + "37.76167" + end;
        //end of location set
				outStr += boundary+end+"Content-Disposition: form-data; name=\"referer\"" + end + end + "juick.com" + end;
				outStr += boundary+end+"Content-Disposition: form-data; name=\"body\"" + end + end + txt + end;
				outStr += boundary + twoHyphens + end;
				var xmlhttp = new XMLHttpRequest();
				if (xmlhttp) {
					xmlhttp.open('POST', 'http://api.juick.com/post', true);
					xmlhttp.setRequestHeader("Connection", "keep-alive");
					xmlhttp.setRequestHeader("Authorization", "Basic " + auth);
					xmlhttp.setRequestHeader("Charset", "UTF-8");
					xmlhttp.setRequestHeader("Content-Type", "multipart/form-data; charset=UTF-8");
					Mojo.Log.info("open connection");
					xmlhttp.onreadystatechange = function(){
						//Mojo.Log.error("state change");
						this.controller=Mojo.Controller.stageController.activeScene();
						if (xmlhttp.readyState == 4) {
							if (xmlhttp.status == 200) {
								Mojo.Log.info("Success:",xmlhttp.responseText);
								spin.style.visible="none";
								spin.mojo.stop();
								newscene.commandMenuModel.items[1].disabled=false;
								//this.controller.modelChanged(newscene.commandMenuModel);
								if (newscene.thread)
								{
									addBookmark(newscene.user,newscene.thread);
									newscene.thread=0;
									newscene.user="";
								}
								Mojo.Controller.stageController.popScene(this.controller, this);
                //this.more=1;
                //var com = new Object();
                //com.command = "do-Refresh";
                //com.type = "mojo-command";
                //com.sender=this;
                //StageAssistant.prototype.handleCommand(com);
							}
							else{
								Mojo.Log.error("Fail:",xmlhttp.responseText);
								spin.style.visible="none";
								spin.mojo.stop();
								newscene.commandMenuModel.items[1].disabled=false;
								//this.controller.modelChanged(newscene.commandMenuModel);
								switch(xmlhttp.status)
								{
									case 0:
										Mojo.Controller.errorDialog("Невозможно подключиться к серверу");
									break;
									case 403:
										Mojo.Controller.errorDialog("Forbidden<br>Неверный логин или пароль"+xmlhttp.statusText);
									break;
									default:
										Mojo.Controller.errorDialog(xmlhttp.status+"<br>"+xmlhttp.statusText);
									break;
								}
							}
						}
					}
					xmlhttp.send(outStr);
				}
				break;
				case "do-Prefs":
					getAuth();
				break;
			case 'do-Home':
				if (p.list == "last") {
					p.list = "home";
					this.controller.get("headertext").update("Моя лента");
					this.controller.get("mainlistid").mojo.revealItem(0,true);
					if(p.messages)
						p.messages.clear();
					p.more=0;
					p.lastthread=0;
					getData(p);
				}
			break;
			case 'do-Last':
				if (p.list == "home") {
					p.list = "last";
					this.controller.get("headertext").update("Последние сообщения");
					this.controller.get("mainlistid").mojo.revealItem(0,true);
					if(p.messages)
						p.messages.clear();
					p.more=0;
					p.lastthread=0;
					getData(p);
				}
			break;
      case 'do-My':
        Mojo.Controller.stageController.pushScene("User", {username:Settings.login,
                            userid:Settings.userid});
      break;


			case "do-Attach":
				var params = {
				actionType: 'attach',
				kinds: ['image','video'],
				onSelect: function(file){
					Mojo.Log.info("picked file",newscene.attach);
					if(!(file.fullPath.match(/.jpg$/i)||file.fullPath.match(/.mp4$/i))){
						Mojo.Controller.errorDialog("Принимаются только фотографии формата jpg и видео mp4");
						return;
					}
					newscene.attach = file.fullPath;
					this.controller=Mojo.Controller.stageController.activeScene();
					var img=this.controller.get("img");
					if(file.fullPath.match(/.jpg$/i))
					{
						img.src=file.fullPath;
						newscene.attachtype="img";
					}
					else
					{
						img.src=file.iconPath;
						newscene.attachtype="video";
					}
					img.parentNode.style.display="block";
					img.style.width="50%";
					img.style.height="75%";

					}

				}
			Mojo.FilePicker.pickFile(params, this.controller.stageController);
			break;
			case "do-About":
				Mojo.Controller.stageController.pushScene("About");
      break;
			case "do-Bookmarks":
				Mojo.Controller.stageController.pushScene("Bookmarks");
			break;
      case "do-Settings":
				Mojo.Controller.stageController.pushScene("Settings");
			break;
			case "do-ClearBookmarks":
        //close websocket connections
        for(var i=0;i<Settings.bookmarks.length;i++)
        {
          WebSocketTest(Settings.bookmarks[i].thread,"disconnect");
        }
				Settings.bookmarks.clear();
				saveSettings();
				this.controller=Mojo.Controller.stageController.activeScene();
				Mojo.Controller.stageController.popScene(this.controller.sceneName);
			break;
			case "do-AddBookmark":
				addBookmark(p.username,p.thread);
			break;
      case "do-Addblack":
        BlacklistAssistant.addWord();
      break;
      case "do-AddPlace":
        newPlace();
      break;
      case "do-tags":
      case "do-authors":
        BlacklistAssistant.switchList(event.command);
      break;
      case 'do-back':
        Mojo.Controller.stageController.popScene(this.controller.sceneName);
      break;
			default:
				break;

        }
    }
    else if(event.type==Mojo.Event.commandEnable)
    {
		Mojo.Log.info("commandEnable");
	}
}
