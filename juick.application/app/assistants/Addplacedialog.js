var self;
var PlaceDialogAssistant = Class.create({
  initialize: function(sceneAssistant) {
    this.sceneAssistant = sceneAssistant;
    this.controller = sceneAssistant.controller;
  },
  location:{},
  setup : function(widget) {
    this.widget = widget;
    this.controller.setupWidget("placeid",
      this.attributes = {
        hintText: "Название",
        multiline: true,
        enterSubmits: true,
        autoReplace: false,
        //textCase: Mojo.Widget.steModeLowerCase,
        focus: true
      },
      this.model = {
        value: "",
        disabled: false
      }
    );
    this.controller.setupWidget("auto",
      this.attributes = {
        type:Mojo.Widget.activityButton
      },
      this.model = {
        label : "Автоназвание",
        disabled: false,
      }
    );
    this.controller.setupWidget("ok",
      this.attributes = {
      },
      this.model = {
        label : "Ok",
        disabled: false,
        buttonClass:"affirmative"
      }
    );
    this.controller.setupWidget("cancel",
      this.attributes = {
      },
      this.model = {
        label : "Cancel",
        disabled: false,
        buttonClass:"negative",
      }
    );
    this.controller.get('auto').addEventListener(Mojo.Event.tap,
      this.handleAutoButton.bind(this));
    this.controller.get('ok').addEventListener(Mojo.Event.tap,
      this.handleButtonok.bind(this));
    this.controller.get('cancel').addEventListener(Mojo.Event.tap,
      this.handleButton.bind(this));
  },
  handleButton: function() {
    this.widget.mojo.close();
  },
  handleButtonok: function() {
    var name=this.controller.get('placeid').mojo.getValue();
    self=this;
    if(name.length<1)return;
    //Settings.password=this.controller.get('passid').mojo.getValue();
    //getUserId();
    this.controller.serviceRequest('palm://com.palm.location', {
     method: 'getCurrentPosition',
     parameters: {},
     onSuccess : function (e){ Mojo.Log.info("getCurrentPosition success, results="+JSON.stringify(e));
       var loc=new Object();
       loc.acc=e.horizAccuracy;
       loc.lat=e.latitude;
       loc.lon=e.longitude;
       loc.pid=0;
       loc.name=name;
       self.addPlace(loc);
       },
     onFailure : function (e){
       Mojo.Log.info("getCurrentPosition failure, results="+JSON.stringify(e));
       Mojo.Controller.errorDialog("Невозможно определить текущее местоположение");
       this.widget.mojo.close();
       }
     });
    //this.widget.mojo.close();
  },
  handleAutoButton: function() {
    this.controller.get('auto').mojo.activate();
    var self=this;
    var location=new Object();
    this.controller.serviceRequest('palm://com.palm.location', {
     method: 'getCurrentPosition',
     parameters: {},
     onSuccess : function (e){ Mojo.Log.info("getCurrentPosition success, results="+JSON.stringify(e));
       location.lat=e.latitude;
       location.lon=e.longitude;
       Mojo.Log.info("open connection to openstreetmap nominatim");
        var request = 'http://nominatim.openstreetmap.org/reverse?format=json&zoom=16&lat='+location.lat+'&lon='+location.lon+'&addressdetails=1&accept-language=ru';
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open('GET', request, true);
        Mojo.Log.info("Request="+request);
        xmlhttp.onreadystatechange = function(){
          if (xmlhttp.readyState == 4) {
            self.controller.get('auto').mojo.deactivate();
            Mojo.Log.info("readystate=4");
            if (xmlhttp.status == 200) {
              Mojo.Log.info("status=200");
              var t=xmlhttp.responseText;
              Mojo.Log.info("Received text="+t);
              //server has wrong response. Some hack.
              var result=t.match(/(\{.*\})/);
              Mojo.Log.info("Result="+result[0]);
              var o=JSON.parse(result[0]);
              Mojo.Log.info(o);
              self.controller.get('placeid').mojo.setValue(o.address.country+', '+o.address.state+', '+o.address.city+', '+o.address.road);
            }
          }
        }
        xmlhttp.send();
      },
     onFailure : function (e){
       Mojo.Log.info("getCurrentPosition failure, results="+JSON.stringify(e));
       Mojo.Controller.errorDialog("Невозможно определить текущее местоположение");
       }
     });

  },
  addPlace: function(loc)
  {
    if(!Settings.login)return;
    var xmlhttp = new XMLHttpRequest();
    var np="";
    if(Settings.login && Settings.password)
    {
      np=Settings.login+":"+Settings.password;
    }
    else{
      getAuth();
      break;
    }
    var auth=Base64.encode(np);
    Mojo.Log.info("open connection");
    var request = 'http://api.juick.com/place_add?name='+loc.name+"&lat="+loc.lat+"&lon="+loc.lon;
    xmlhttp.open('GET', request, true);
    xmlhttp.setRequestHeader("Authorization", "Basic " + auth);
    Mojo.Log.info("Request="+request);
    xmlhttp.onreadystatechange = function(){
      if (xmlhttp.readyState == 4) {
        Mojo.Log.info("readystate=4");
        if (xmlhttp.status == 200) {
          Mojo.Log.info("status=200");
          var t=xmlhttp.responseText;
          Mojo.Log.info("Received text="+t);
          //server has wrong response. Some hack.
          var result=t.match(/(\{.*\})/);
          Mojo.Log.info("Result="+result[0]);
          var o=JSON.parse(result[0]);
          Mojo.Log.info(o);
          //Settings.places=o.pid;
          loc.pid=o.pid;
          Mojo.Log.info("loc=",JSON.stringify(loc));
          Mojo.Log.info("name=",name);
          Settings.places.push(loc);
          //Mojo.Log.info("UserID="+Settings.userid);
          saveSettings();
        }
        else {
          switch(xmlhttp.status)
          {
            case 0:
              Mojo.Controller.errorDialog("Невозможно подключиться к сервру");
            break;
            case 403:
              Mojo.Controller.errorDialog("Forbidden<br>Неверный логин или пароль"+xmlhttp.statusText);
            break;
            case 404:
              //If this is new place then server returns error 404. All right.
            break;
            default:
              Mojo.Controller.errorDialog(xmlhttp.status+"<br>"+xmlhttp.statusText);
            break;
          }
        }
      }
      self.widget.mojo.close();
      var controller=Mojo.Controller.stageController.activeScene();
      Mojo.Log.info("Scene name=",controller.sceneName);
      controller.get('mainlistid').mojo.noticeUpdatedItems(0,Settings.places);
    }
    xmlhttp.send();
  }

});
